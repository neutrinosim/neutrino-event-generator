/*
 * particle.cpp
 *
 *  Created on: 8 Feb 2021
 *      Author: jack
 */


#include "particle.h"

using namespace UTILS;

Particle::Particle(const Flavour& type, const Vec4<double>& k, decay_state initial){

	this->type = new Flavour(type);
	this->k = new Vec4<double>(k);
	this->state = initial;

}
Particle::Particle(long int pdg_code, const Vec4<double>& k, decay_state initial){

	this->type = new Flavour(pdg_code);
	this->k = new Vec4<double>(k);
	this->state = initial;

}
Particle::Particle(Particle& particle){
	type = new Flavour(particle.get_flavour());
	k = new Vec4<double>(particle.get_4mom());
	state = decay_state(particle.has_decayed());

}
Flavour const Particle::get_flavour() const{
	return *this->type;
}
Vec4<double> Particle::get_4mom() const{
	return *this->k;
}
bool const Particle::has_decayed() const{
	if (this->state == decayed){
		return true;
	}else{
		return false;
	}
}
double Particle::mass() const{
	return type->get_mass();
}
