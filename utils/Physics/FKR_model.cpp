/*
 * FKR_model.cpp
 *
 *  Created on: 20 Mar 2021
 *      Author: jack
 */

#include "FKR_model.h"

using namespace UTILS;

FKR_model::FKR_model(Flavour in_hadron, double M_V, double M_A){

	this->in_hadron = new Flavour(in_hadron);
	this->GV_ff = new Form_Factor_Dipole(1., M_V);
	this->GA_ff = new Form_Factor_Dipole(1., M_A);

}

FKR_model::~FKR_model(){};

double FKR_model::lambda(double W2, double q_mom){

	double lambda = sqrt(2./(W2*omega))*this->in_hadron->get_mass()*q_mom;

	return lambda;
}

double FKR_model::R(double W2, double q_mom, double Q2, double n){

	double W = sqrt(W2);
	double R = sqrt(2.)*this->in_hadron->get_mass()*(W+this->in_hadron->get_mass())*q_mom/(W*(pow(W+this->in_hadron->get_mass(),2) + Q2));
	R *= G_V(Q2, n);

	return R;
}

double FKR_model::R_A(double W2, double Q2, double n){

	double W = sqrt(W2);
	double R_A = (this->Z*sqrt(2)/(6*W)) * (W + this->in_hadron->get_mass() + (2*n*omega*W/(pow(W+this->in_hadron->get_mass(),2) + Q2)));
	R_A *= G_A(Q2, n);

	return R_A;
}

double FKR_model::S(double W2, double q_mom, double Q2, double n){

	double W = sqrt(W2);

	double S = (Q2/pow(q_mom,2)) * (3*W*in_hadron->get_mass() - Q2 - pow(in_hadron->get_mass(), 2)) / (6*pow(in_hadron->get_mass(), 2));
	S *= G_V(Q2, n);

	return S;
}

double FKR_model::B(double W2, double Q2, double n){

	double W = sqrt(W2);

	double B = (Z/(3*W))*sqrt(omega/2)*( 1 + (W2 - pow(in_hadron->get_mass(),2) - Q2)/(pow(W + in_hadron->get_mass(),2) + Q2)) * G_A(Q2, n);

	return B;
}

double FKR_model::C(double W2, double q_mom, double Q2, double n){

	double W = sqrt(W2);
	double C = Z * (W2 - pow(in_hadron->get_mass(),2) + n*omega*(W2 - pow(in_hadron->get_mass(),2) - Q2)/(pow(W + in_hadron->get_mass(),2) + Q2));
	C *= G_A(Q2, n);

	return C;
}

double FKR_model::T(double W2, double Q2, double n){
	double T = sqrt(omega/2)*G_V(Q2, n)/(3.*sqrt(W2));

	return T;
}

double FKR_model::T_A(double W2, double q_mom, double Q2, double n){
	double T_A = (2./3)*Z*sqrt(omega/2)*(in_hadron->get_mass()/sqrt(W2))*(q_mom/(pow(sqrt(W2)+in_hadron->get_mass(),2) + Q2))*G_A(Q2, n);

	return T_A;
}

//R+ and R- for simplification of helicity amplitudes

double FKR_model::R_plus(double W2, double q_mom, double Q2, double n){

	return -1*(R(W2, q_mom, Q2, n) + R_A(W2, Q2, n));
}

double FKR_model::R_minus(double W2, double q_mom, double Q2, double n){

	return -1*(R(W2, q_mom, Q2, n) - R_A(W2, Q2, n));
}

double FKR_model::T_plus(double W2, double q_mom, double Q2, double n){

	return -1*(T(W2, Q2, n) + T_A(W2, q_mom, Q2, n));
}

double FKR_model::T_minus(double W2, double q_mom, double Q2, double n){

	return -1*(T(W2, Q2, n) - T_A(W2, q_mom, Q2, n));
}

//Modified form factors

double FKR_model::G_V(double Q2, double n){

	double tau = Q2/(4*pow(in_hadron->get_mass(),2));

	return pow(1 + tau, 0.5-n) * this->GV_ff->calc_ff(Q2);
}

double FKR_model::G_A(double Q2, double n){

	double tau = Q2/(4*pow(in_hadron->get_mass(),2));

	return pow(1 + tau, 0.5-n) * this->GA_ff->calc_ff(Q2);
}
