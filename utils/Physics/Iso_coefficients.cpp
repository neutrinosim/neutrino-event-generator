/*
 * Iso_coefficients.cpp
 *
 *  Created on: 25 Mar 2021
 *      Author: jack
 */

#include "Iso_coefficients.h"

using namespace UTILS;

Iso_coefficients::Iso_coefficients(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, const Flavour& out_pion){

	this->in_hadron = new Flavour(in_hadron);
	this->out_hadron = new Flavour(out_hadron);
	this->out_pion = new Flavour(out_pion);
}

Iso_coefficients::~Iso_coefficients(){}

double Iso_coefficients::get_coefficients(int I){
	double C=0.;
	//coefficients from MK2018
	if(in_hadron->get_code()==proton){
		if(out_hadron->get_code()==proton && out_pion->get_code()==pion_p){
			if(I==1){
				C=0.;
			}else
			if(I==3){
				C=1.;
			}
		}else
		if(out_hadron->get_code()==proton && out_pion->get_code()==pion_z){
			if(I==1){
				C=-sqrt(1./3);
			}else
			if(I==3){
				C=sqrt(2./3);
			}
		}else
		if(out_hadron->get_code()==neutron && out_pion->get_code()==pion_p){
			if(I==1){
				C=-sqrt(2./3);
			}else
			if(I==3){
				C=-sqrt(1./3);
			}
		}
	}else
	if(in_hadron->get_code()==neutron){
		if(out_hadron->get_code()==proton && out_pion->get_code()==pion_z){
			if(I==1){
				C=sqrt(1./3);
			}else
			if(I==3){
				C=-sqrt(2./3);
			}
		}else
		if(out_hadron->get_code()==neutron && out_pion->get_code()==pion_p){
			if(I==1){
				C=sqrt(2./3);
			}else
			if(I==3){
				C=sqrt(1./3);
			}
		}else
		if(out_hadron->get_code()==neutron && out_pion->get_code()==pion_z){
			if(I==1){
				C=sqrt(1./3);
			}else
			if(I==3){
				C=sqrt(2./3);
			}
		}else
		if(out_hadron->get_code()==proton && out_pion->get_code()==pion_m){
			if(I==1){
				C=-sqrt(2./3);
			}else
			if(I==3){
				C=sqrt(1./3);
			}
		}
	}

	return C;
}

