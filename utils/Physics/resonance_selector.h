/*
 * resonance_selector.h
 *
 *  Created on: 20 Mar 2021
 *      Author: jack
 */

#ifndef RESONANCE_SELECTOR_H_
#define RESONANCE_SELECTOR_H_

#include "process_type.h"
#include "FKR_model.h"
#include "physics_constants.h"

#include <vector>
#include <complex>

namespace UTILS{

enum partial_wave{

	P,
	D,
	S,
	F,
};

static const partial_wave all_partial_waves[] = {P, D, S, F};

enum p_m{plus, minus,};

class Resonance_selector{

private:

	process_type boson;

	int res_charge;

	FKR_model *model;

	constants consts = constants();

public:

	Resonance_selector(process_type boson, Flavour in_hadron, int res_charge, double M_V, double M_A);

	~Resonance_selector();

	double hel_amps(Flavour& resonance, int jz, p_m sign, double W2, double q_mom, double Q2); //returns hel amps for jz +/-

	std::vector<Flavour> get_resonances(int I);

	std::vector<Flavour> get_resonances();

	double BW_2(Flavour& res, double W2);

	std::complex<double> BW(Flavour& res, double W2);

	int get_decay_sign(Flavour& res);

};


}

#endif /* RESONANCE_SELECTOR_H_ */
