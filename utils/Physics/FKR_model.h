/*
 * FKR_model.h
 *
 *  Created on: 20 Mar 2021
 *      Author: jack
 */

#ifndef FKR_MODEL_H_
#define FKR_MODEL_H_

#include "form_factor.h"
#include "flavour.h"

namespace UTILS{

class FKR_model{

private:

	double omega = 1.05; //FeynmanKislingerRavndal171
	double Z = 0.75; //Ravndal1973

	Flavour *in_hadron;

	Form_Factor_Dipole *GA_ff, *GV_ff;

public:

	FKR_model(Flavour in_hadron, double M_V, double M_A);

	~FKR_model();

//Creating functions for the FKR model coefficients from ReinSehgal

	double lambda(double W2, double q_mom);

	double R(double W2, double q_mom, double Q2, double n);

	double R_A(double W2, double Q2, double n);

	double S(double W2, double q_mom, double Q2, double n);

	double B(double W2, double Q2, double n);

	double C(double W2, double q_mom, double Q2, double n);

	double T(double W2, double Q2, double n);

	double T_A(double W2, double q_mom, double Q2, double n);

//Combination of RV and RA from Ravndal & ReinSehgal

	double R_plus(double W2, double q_mom, double Q2, double n);

	double R_minus(double W2, double q_mom, double Q2, double n);

	double T_plus(double W2, double q_mom, double Q2, double n);

	double T_minus(double W2, double q_mom, double Q2, double n);

//G_V/A form factors need modifying from generic dipole form factors, which in turn depends on no. oscillators in res.

	double G_V(double Q2, double n);

	double G_A(double Q2, double n);

};
}


#endif /* FKR_MODEL_H_ */
