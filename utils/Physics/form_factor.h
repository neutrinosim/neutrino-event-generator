/*
 * form_factor.h
 *
 *  Created on: Nov 10, 2020
 *      Author: jack
 */

#ifndef FORM_FACTOR_H_
#define FORM_FACTOR_H_

#include <math.h>

using namespace std;

namespace UTILS{

class Form_Factor{
private:

public:

	double F0;

	Form_Factor(double F0){this->F0 = F0;};

	virtual ~Form_Factor(){};

	virtual double calc_ff(double Q2){return this->F0;};

};

class Form_Factor_Dipole: public Form_Factor{
private:

	double dipole_mass;

public:

	Form_Factor_Dipole(double F0, double dipole_mass): Form_Factor(F0){
		this->dipole_mass = dipole_mass;
	}

	~Form_Factor_Dipole(){};

	double calc_ff(double Q2){
		return this->F0*pow((1 + Q2/pow(this->dipole_mass,2)), -2);
	}
};

class Double_Dipole: public Form_Factor{
private:
	double M1, M2;

public:

	Double_Dipole(double F0, double M1, double M2): Form_Factor(F0){
		this->M1 = M1;
		this->M2 = M2;
	}

	~Double_Dipole(){};

	double calc_ff(double Q2){
		return pow((1 + Q2/pow(this->M1,2)), -2)*pow((1 + Q2/pow(this->M2,2)), -2)*this->F0;
	}
};

}
#endif /* FORM_FACTOR_H_ */
