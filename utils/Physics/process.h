/*
 * inv_amp.h
 *
 *  Created on: 22 Sep 2020
 *      Author: jack
 */

#ifndef SIM_PROCESS_H_
#define SIM_PROCESS_H_

#include "../Maths/phase_space.h"
#include "../Physics/flavour.h"
#include "../Maths/vec4.h"
#include "me_2.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/GenVertex_fwd.h"
#include "HepMC3/GenEvent.h"
#include "HepMC3/WriterAsciiHepMC2.h"
#include "HepMC3/GenCrossSection.h"
#include "HepMC3/GenCrossSection_fwd.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <random>

namespace UTILS{

class Process{

protected:
	Flavour *in_neutrino, *in_hadron, *out_lepton;
	Phase_space<double> *p_space;
	double max_cross = 0;
	int N_evnts = 0;

	virtual HepMC3::GenVertexPtr gen_interaction(const std::vector<Vec4<double>> moms);

	virtual std::vector<Vec4<double>> get_particle_moms();

public:

	Process(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton, const Variable<double>& neutrino_spectrum, const Variable<double>& costheta);

	virtual ~Process();

	virtual double calc_diffcross(const std::vector<Vec4<double>> moms);

	HepMC3::GenCrossSection calc_cross(int o_mag = -3);

	virtual void gen_events(int N_events=1, std::string out_file="./output/test.txt");

};

}


#endif /* SIM_PROCESS_H_ */
