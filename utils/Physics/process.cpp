/*
 * process.cpp
 *
 *  Created on: 8 Feb 2021
 *      Author: jack
 */


#include "process.h"

using namespace UTILS;

Process::Process(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton, const Variable<double>& neutrino_spectrum, const Variable<double>& costheta){

	this->in_neutrino = new Flavour(in_neutrino);
	this->in_hadron = new Flavour(in_hadron);
	this->out_lepton = new Flavour(out_lepton) ;

	this->p_space = new Phase_space<double>(neutrino_spectrum);
	this->p_space->add_variable(costheta);
}

Process::~Process(){};

double Process::calc_diffcross(const std::vector<Vec4<double>> moms){return 0;}

HepMC3::GenVertexPtr Process::gen_interaction(const std::vector<Vec4<double>> moms){

	Vec4<double> k_i = moms[0];
	Vec4<double> k_f = moms[1];
	Vec4<double> p_i = moms[2];

	HepMC3::FourVector i_lep_mom = HepMC3::FourVector(k_i[1], k_i[2], k_i[3], k_i[0]);
	HepMC3::FourVector o_lep_mom = HepMC3::FourVector(k_f[1], k_f[2], k_f[3], k_f[0]);
	HepMC3::FourVector i_had_mom = HepMC3::FourVector(p_i[1], p_i[2], p_i[3], p_i[0]);

	HepMC3::GenParticlePtr i_lep(new HepMC3::GenParticle(i_lep_mom, this->in_neutrino->get_pdg_int()));
	HepMC3::GenParticlePtr o_lep(new HepMC3::GenParticle(o_lep_mom, this->out_lepton->get_pdg_int()));
	HepMC3::GenParticlePtr i_had(new HepMC3::GenParticle(i_had_mom, this->in_hadron->get_pdg_int()));

	HepMC3::GenVertexPtr interaction(new HepMC3::GenVertex());

	interaction->add_particle_in(i_lep);
	interaction->add_particle_in(i_had);
	interaction->add_particle_out(o_lep);

	return interaction;

}

std::vector<Vec4<double>> Process::get_particle_moms(){
	std::vector<Vec4<double>> nothing;
	nothing.emplace_back(0,0,0,0);
	nothing.emplace_back(0,0,0,0);
	nothing.emplace_back(0,0,0,0);
	return nothing;
}

HepMC3::GenCrossSection Process::calc_cross(int o_mag){
	HepMC3::GenCrossSection cross_sec;

	double max_cross=0, sum=0, sum_2=0, diff_cross, error, mean;
	double N=0.;
	Vec4<double> k_i, k_f, p_i;

	do{

		diff_cross = this->calc_diffcross(this->get_particle_moms());
		sum += diff_cross;
		sum_2 += pow(diff_cross, 2);
		N += 1.;

		if(diff_cross > max_cross){max_cross = diff_cross;}
		mean = sum/N;
		error = std::sqrt(((sum_2/N)-std::pow(mean,2))/(N-1)); //standard error from H&H

	}while(error/mean > pow(10, o_mag) or N<1000);

	if(max_cross > this->max_cross){this->max_cross = max_cross;}

	cross_sec.set_cross_section(mean*this->p_space->volume(), error);

	this->N_evnts = N;

	std::cout<< "Cross section calculated using: " << N <<"points" <<"\n";

	return cross_sec;
}

void Process::gen_events(int N_events, std::string out_file){
	HepMC3::GenCrossSectionPtr cross_sec(new HepMC3::GenCrossSection(this->calc_cross()));					//Will attach to generated events
	//Need max_cross for "hit-or-miss"

	HepMC3::WriterAsciiHepMC2 writer(out_file);

	std::mt19937_64 generator = std::mt19937_64();
	std::uniform_real_distribution<double> dist = std::uniform_real_distribution<double>(0.0, 1.0);

	std::vector<Vec4<double>> part_moms;
	double diff_cross, r, ratio;

	HepMC3::GenEvent *evt;
	HepMC3::GenVertexPtr interaction;

	for(int i=0;i<N_events;i++){
		bool hit = false;

		while(hit==false){

			part_moms = this->get_particle_moms();
			diff_cross = this->calc_diffcross(part_moms);

			r = dist(generator);
			ratio = diff_cross/this->max_cross;

			if(ratio >= r){	//Check if event is a "hit" or "miss"
				hit = true;
			}
		}

		evt = new HepMC3::GenEvent();
		interaction = this->gen_interaction(part_moms);

		evt->add_vertex(interaction);
		evt->set_cross_section(cross_sec);
		writer.write_event(*evt);

	}
}

