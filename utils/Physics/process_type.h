/*
 * process_type.h
 *
 *  Created on: 20 Mar 2021
 *      Author: jack
 */

#ifndef PROCESS_TYPE_H_
#define PROCESS_TYPE_H_

enum process_type{

	CC, //weak charged current
	NC, //weak neutral current
	EM, //emag process

};


#endif /* PROCESS_TYPE_H_ */
