/*
 * me_2.cpp
 *
 *  Created on: 8 Feb 2021
 *      Author: jack
 */

#include "me_2.h"

using namespace UTILS;

ME_2::~ME_2(){};

ME_2::ME_2(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton){

	this->in_neutrino = new Flavour(in_neutrino);
	this->in_hadron = new Flavour(in_hadron);
	this->out_lepton = new Flavour(out_lepton);
}

double const ME_2::calc_q2(const Particle& in_neutrino, const Particle& out_lepton){
	Vec4<double> q = in_neutrino.get_4mom();
	q -=  out_lepton.get_4mom();
	return q*q;
}

double const ME_2::calc_ME_2 (const std::vector<Vec4<double>> moms){
		return 0;
};

bool const ME_2::check_cons(){return true;}
