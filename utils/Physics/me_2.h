/*
 * ME_2.h
 *
 *  Created on: 22 Sep 2020
 *      Author: jack
 *
 *  Defines a class which contains all 4 vectors in a scattering ME_2
 */

#ifndef UTILS_PHYSICS_ME_2_H_
#define UTILS_PHYSICS_ME_2_H_

#include <vector>
#include "../Maths/vec4.h"
#include "particle.h"
#include "form_factor.h"

namespace UTILS{

enum k_comb{
		kiki = 0, kipi = 1, kikf = 2, kipf = 3, kiq = 4,
		piki = 1, pipi = 5, pikf = 6, pipf = 7, piq = 8,
		kfki = 2, kfpi = 6, kfkf = 9, kfpf = 10, kfq = 11,
		pfki = 3, pfpi = 7, pfkf = 10, pfpf = 12, pfq = 13,
		qki = 4, qpi = 8, qkf = 11, qpf = 13, qq = 14
	};

class ME_2{
//Calculates the matrix element squared for given in/out particles
protected:
	Flavour *in_neutrino, *in_hadron, *out_lepton;

	const double G_F = 1.1663787 * pow(10.0, -5.0); //Units: GeV^-2, Ref: nist

public:
	ME_2(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton);
	virtual ~ME_2();

	virtual double const calc_ME_2 (const std::vector<Vec4<double>> moms);

	virtual bool const check_cons();

	double const calc_q2(const Particle& in_neutrino, const Particle& out_lepton);
};
}
#endif /* UTILS_PHYSICS_ME_2_H_ */
