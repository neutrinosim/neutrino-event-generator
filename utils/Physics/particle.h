/*
 * particle.h
 *
 *  Created on: Nov 10, 2020
 *      Author: jack
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include "flavour.h"
#include "../Maths/vec4.h"
#include <vector>

namespace UTILS{

enum decay_state{
	active = 0,
	decayed = 1
};

class Particle{
private:

	Flavour *type;
	Vec4<double> *k;
	decay_state state;

public:

	Particle(const Flavour& type, const Vec4<double>& k, decay_state initial);

	Particle(long int pdg_code, const Vec4<double>& k, decay_state initial);

	Particle(Particle& particle);

	Flavour const get_flavour() const;
	Vec4<double> get_4mom() const;
	bool const has_decayed() const;

	double mass() const;

};

}

#endif /* PARTICLE_H_ */
