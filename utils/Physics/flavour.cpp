/*
 * flavour.cpp
 *
 *  Created on: 8 Feb 2021
 *      Author: jack
 */

#include "flavour.h"
#include <iostream>

using namespace UTILS;

Flavour::Flavour(pdg_code code){
	this->code = code;

	std::tuple<double, double, double, double, int, int, int> vals = this->flavour_vals(this->code);

	this->mass = std::get<0>(vals);
	this->width = std::get<1>(vals);
	this->charge = std::get<2>(vals);
	this->mag_dipole = std::get<3>(vals);
	this->I = std::get<4>(vals);
	this->J = std::get<5>(vals);
	this->L = std::get<6>(vals);

}
Flavour::Flavour(long int int_code){
	this->code = pdg_code(int_code);

	std::tuple<double, double, double, double, int, int, int> vals = this->flavour_vals(this->code);

	this->mass = std::get<0>(vals);
	this->width = std::get<1>(vals);
	this->charge = std::get<2>(vals);
	this->mag_dipole = std::get<3>(vals);
	this->I = std::get<4>(vals);
	this->J = std::get<5>(vals);
	this->L = std::get<6>(vals);
}
Flavour::Flavour(const Flavour& type){

	this->code = type.get_code();
	this->mass = type.get_mass();
	this->width = type.get_width();
	this->charge = type.get_charge();
	this->mag_dipole = type.get_m_dipole();
	this->I = type.get_I();
	this->J = type.get_J();
	this->L = type.get_L();

}

pdg_code Flavour::get_code() const{
	return pdg_code(this->code);
}

int Flavour::get_pdg_int() const {
	int pdg_int = this->code;
	return pdg_int;
}

double Flavour::get_mass() const{return this->mass;}

double Flavour::get_width() const {return this->width;}

double Flavour::get_charge() const {return this->charge;}

double Flavour::get_m_dipole() const {return this->mag_dipole;}

int Flavour::get_I() const {return this->I;}

int Flavour::get_J() const {return this->J;}

int Flavour::get_L() const {return this->L;}

std::tuple<double, double, double, double, int, int, int> Flavour::flavour_vals(pdg_code code){
	//returns values for a given flavour, namely: mass, width, charge, magnetic moment, isospin, spin
	//all values are from PDG-Live, charge is in units of absolute electron charge
	std::tuple<double, double, double, double, int, int, int> vals;

	switch(code){

	case(electron):
		vals = std::make_tuple(0.0005109989, 0., -1., 0., -1, 1, 0);
		break;
	case(positron):
		vals = std::make_tuple(0.0005109989, 0., 1., 0., 1, 1, 0);
		break;
	case(electron_nu):
		vals = std::make_tuple(0., 0., 0., 0., 1, 1, 0);
			break;
	case(electron_anu):
		vals = std::make_tuple(0., 0., 0., 0., -1, 0, 0);
			break;
	case(muon):
		vals = std::make_tuple(0.1056583, 0., -1., 0., -1, 1, 0);
			break;
	case(antimuon):
		vals = std::make_tuple(0.1056583, 0., 1., 0., 1, 1, 0);
			break;
	case(muon_nu):
		vals = std::make_tuple(0., 0., 0., 0., 1, 1, 0);
			break;
	case(muon_anu):
		vals = std::make_tuple(0., 0., 0., 0., -1, 1, 0);
			break;
	case(tau):
		vals = 	std::make_tuple(1.777, 0., -1., 0., -1, 1, 0);
			break;
	case(antitau):
		vals = 	std::make_tuple(1.777, 0., 1., 0., 1, 1, 0);
			break;
	case(tau_nu):
		vals = std::make_tuple(0., 0., 0., 0., 1, 1, 0);
			break;
	case(tau_anu):
		vals = std::make_tuple(0., 0., 0., 0., -1, 1, 0);
			break;
	case(proton):
		vals = std::make_tuple(0.9382720, 0., 1., 2.79284734, 1, 1, 0);
			break;
	case(neutron):
		vals = std::make_tuple(0.9395654, 0., 0., -1.91304, 1, 1, 0);
			break;
	case(delta_pp):
		vals = std::make_tuple(1.232, 0.117, 2., 6.14, 3, 3, 1);
	//Castro2000 DOI:10.1016/S0370-2693(01)00980-7 for mag moment
			break;
	case(delta_p):
		vals = std::make_tuple(1.232, 0.117, 1., 2.7, 3, 3, 1);
	//Kotulla2002 for mag moment
			break;
	case(delta_z):
		vals = std::make_tuple(1.232, 0.117, 0., 0.0, 3, 3, 1);
			break;
	case(delta_m):
		vals = std::make_tuple(1.232, 0.117, -1., 2.7, 3, 3, 1);
			break;
	case(lambda):
		vals = std::make_tuple(1.116, 0.0, 0., -0.613, 0, 1, 0);
			break;
	case(p_1440):
		vals = std::make_tuple(1.44, 0.35, 1., 0., 1, 1, 1);
			break;
	case(n_1440):
		vals = std::make_tuple(1.44, 0.35, 0., 0., 1, 1, 1);
			break;
	case(p_1520):
		vals = std::make_tuple(1.515, 0.11, 1., 0., 1, 3, 2);
			break;
	case(n_1520):
		vals = std::make_tuple(1.515, 0.11, 0., 0., 1, 3, 2);
			break;
	case(pion_z):
		vals = std::make_tuple(0.135, 0., 0., 0., 1, 0, 0);
			break;
	case(pion_p):
		vals = std::make_tuple(0.1396, 0., 1., 0., 1, 0, 0);
			break;
	case(pion_m):
		vals = std::make_tuple(0.1396, 0., -1., 0., 1, 0, 0);
			break;
	/*
	default:
		vals.push_back(0.0);
		vals.push_back(0.0);
		vals.push_back(0.0);
		vals.push_back(0.0);
		break;*/
	}

	return vals;
}

