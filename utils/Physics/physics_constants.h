/*
 * physics_constants.h
 *
 *  Created on: 20 Mar 2021
 *      Author: jack
 */

#ifndef PHYSICS_CONSTANTS_H_
#define PHYSICS_CONSTANTS_H_

class constants{
public:

	constants(){};
	~constants(){};

	const double sin_2_theta_w = 0.23;
	const double G_F = 1.1663787 * pow(10.0, -5.0); //Units: GeV^-2, Ref: nist
	const double Vud = 0.9737;

};



#endif /* PHYSICS_CONSTANTS_H_ */
