/*
 * resonance_selector.cpp
 *
 *  Created on: 20 Mar 2021
 *      Author: jack
 */

#include "resonance_selector.h"

using namespace UTILS;

Resonance_selector::Resonance_selector(process_type boson, Flavour in_hadron, int res_charge, double M_V, double M_A){

	this->boson = boson;

	this->model = new FKR_model(in_hadron, M_V, M_A);

	this->res_charge = res_charge;

}

double Resonance_selector::hel_amps(Flavour& resonance, int jz, p_m sign, double W2, double q_mom, double Q2){
//returns specific helicity amplitudes for a particular resonance across resonant masses

	double hel_amps;
	double x = this->consts.sin_2_theta_w;
	double n;

	switch(resonance.get_code()){

	case(p_1440):
		n=2;
		if(jz==1&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(0.75) * pow(this->model->lambda(W2, q_mom),2) * this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -(5./6)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = (5./12)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * (this->model->R_minus(W2, q_mom, Q2, n) +
						12.*x*this->model->R(W2, q_mom, Q2, n)/5);
			}
		}else
		if(jz==1&&sign==plus){
			if(this->boson == EM){
				hel_amps = -sqrt(0.75) * pow(this->model->lambda(W2, q_mom),2) * this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -(5./6)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = (5./12)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * (this->model->R_plus(W2, q_mom, Q2, n) +
						12.*x*this->model->R(W2, q_mom, Q2, n)/5);
			}
		}else
		if(jz==0&&sign==plus){
			if(this->boson == EM){
				hel_amps = -sqrt(0.75) * pow(this->model->lambda(W2, q_mom),2) * this->model->S(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(3./4) * pow(this->model->lambda(W2, q_mom),2) * this->model->S(W2, q_mom, Q2, n) - (5./6)*sqrt(3.)*this->model->lambda(W2, q_mom)*(
						this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2.*this->model->B(W2, Q2, n));
			}
			else if(this->boson == NC){
				hel_amps = 0.5*sqrt(3./4)*pow(this->model->lambda(W2, q_mom),2)*this->model->S(W2, q_mom, Q2, n)*(1.-4.*x) - (5./12)*sqrt(3.)*this->model->lambda(W2, q_mom)*
						(this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2.*this->model->B(W2, Q2, n));
			}
		}else
		if(jz==0&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(0.75) * pow(this->model->lambda(W2, q_mom),2) * this->model->S(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(3./4) * pow(this->model->lambda(W2, q_mom),2) * this->model->S(W2, q_mom, Q2, n) + (5/6)*sqrt(3.)*this->model->lambda(W2, q_mom)*(
						this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2*this->model->B(W2, Q2, n));
			}
			else if(this->boson == NC){
				hel_amps = 0.5*sqrt(3/4)*pow(this->model->lambda(W2, q_mom),2)*this->model->S(W2, q_mom, Q2, n)*(1-4*x) + (5/12)*sqrt(3)*this->model->lambda(W2, q_mom)*
						(this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2*this->model->B(W2, Q2, n));
			}
		}
		else{
			hel_amps = 0.;
		}
		break;
	case(n_1440):
		n=2;
		if(jz==1&&sign==minus){
			if(this->boson == EM){
				hel_amps = sqrt(1./3) * pow(this->model->lambda(W2, q_mom),2) * this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -(5./6)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = (5./12)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * (this->model->R_minus(W2, q_mom, Q2, n) +
						8.*x*this->model->R(W2, q_mom, Q2, n)/5);
			}
		}else
		if(jz==1&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(1./3) * pow(this->model->lambda(W2, q_mom),2) * this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -(5./6)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = (5./12)*sqrt(3.) * pow(this->model->lambda(W2, q_mom),2) * (this->model->R_plus(W2, q_mom, Q2, n) +
						8.*x*this->model->R(W2, q_mom, Q2, n)/5);
			}
		}else
		if(jz==0&&sign==plus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(3./4) * pow(this->model->lambda(W2, q_mom),2) * this->model->S(W2, q_mom, Q2, n) - (5/6)*sqrt(3)*this->model->lambda(W2, q_mom)*(
						this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2*this->model->B(W2, Q2, n));
			}
			else if(this->boson == NC){
				hel_amps = 0.5*sqrt(3./4)*pow(this->model->lambda(W2, q_mom),2)*this->model->S(W2, q_mom, Q2, n)*(1.-4.*x) + (5./12)*sqrt(3.)*this->model->lambda(W2, q_mom)*
						(this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2.*this->model->B(W2, Q2, n));
			}
		}else
		if(jz==0&&sign==minus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(3./4) * pow(this->model->lambda(W2, q_mom),2) * this->model->S(W2, q_mom, Q2, n) + (5./6)*sqrt(3.)*this->model->lambda(W2, q_mom)*(
						this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2.*this->model->B(W2, Q2, n));
			}
			else if(this->boson == NC){
				hel_amps = 0.5*sqrt(3./4)*pow(this->model->lambda(W2, q_mom),2)*this->model->S(W2, q_mom, Q2, n)*(1.-4.*x) - (5./12)*sqrt(3.)*this->model->lambda(W2, q_mom)*
						(this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n) - 2.*this->model->B(W2, Q2, n));
			}
		}else{
			hel_amps = 0.;
		}
		break;

	case(delta_pp):
		n=0;
		if(jz==3&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(6.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(6.)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(6.)*(this->model->R_minus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==1&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(2.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(2.)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(2.)*(this->model->R_minus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==1&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(2.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(2.)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(2.)*(this->model->R_plus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==3&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(6.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(6.)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(2.)*(this->model->R_plus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==0&&sign==plus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = 2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
		}else
		if(jz==0&&sign==minus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = 2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
		}
		else{
			hel_amps = 0.;
		}
		break;
	case(delta_p):
		n=0;
		if(jz==3&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(6.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(6.)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(6.)*(this->model->R_minus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==1&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(2.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(2.)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(2.)*(this->model->R_minus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==1&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(2.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(2.)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(2.)*(this->model->R_plus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==3&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(6.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(6.)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(2.)*(this->model->R_plus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==0&&sign==plus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = 2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
		}else
		if(jz==0&&sign==minus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = 2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
		}
		else{
			hel_amps = 0.;
		}
		break;
	case(delta_z):
		n=0;
		if(jz==3&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(6.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(6.)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(6.)*(this->model->R_minus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==1&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(2.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(2.)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(2.)*(this->model->R_minus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==1&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(2.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(2.)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(2.)*(this->model->R_plus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==3&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(6.)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -sqrt(6.)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(6.)*(this->model->R_plus(W2, q_mom, Q2, n) + 2.*x*this->model->R(W2, q_mom, Q2, n));
			}
		}else
		if(jz==0&&sign==plus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = 2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
		}else
		if(jz==0&&sign==minus){
			if(this->boson == EM){
				hel_amps = 0.;
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -2.*sqrt(2.)*this->model->C(W2, q_mom, Q2, n);
			}
		}
		else{
			hel_amps = 0.;
		}
		break;
	case(p_1520):
		n=1;
		if(jz==3&&sign==minus){
			if(this->boson == EM){
				hel_amps = sqrt(9./2)*this->model->T(W2, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = 3.*sqrt(2.)*this->model->T_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(9./2)*(this->model->T_minus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n));
			}
		}
		else if(jz==1&&sign==minus){
			if(this->boson == EM){
				hel_amps = sqrt(3./2)*this->model->T(W2, Q2, n) - sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(6.)*this->model->T_minus(W2, q_mom, Q2, n) - (4./3)*sqrt(3)*this->model->lambda(W2, q_mom)*this->model->R_minus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(3./2)*(this->model->T_minus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n)) -
						sqrt(4./3)*this->model->lambda(W2, q_mom)*(this->model->R_minus(W2, q_mom, Q2, n) + 3*x*this->model->R(W2, q_mom, Q2, n));
			}
		}
		else if(jz==1&&sign==plus){
			if (this->boson == EM){
				hel_amps = sqrt(3./2)*this->model->T(W2, Q2, n) - sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->R(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = sqrt(6.)*this->model->T_plus(W2, q_mom, Q2, n) - (4./3)*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->R_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(3./2)*(this->model->T_plus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n)) -
						sqrt(4./3)*this->model->lambda(W2, q_mom)*(this->model->R_plus(W2, q_mom, Q2, n) + 3*x*this->model->R(W2, q_mom, Q2, n));
			}
		}
		else if(jz==3&&sign==plus){
			if(this->boson == EM){
				hel_amps = sqrt(9./2)*this->model->T(W2, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = 3.*sqrt(2.)*this->model->T_plus(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = sqrt(9./2)*(this->model->T_plus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n));
			}
		}
		else if(jz==0&&sign==plus){
			if(this->boson == EM){
				hel_amps = -sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n) - (4./3)*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n)*(1-2*x) - 2.*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n)/sqrt(3.);
			}
		}
		else if(jz==0&&sign==minus){
			if(this->boson == EM){
				hel_amps = -sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n);
			}
			else if(this->boson == CC){
				hel_amps = -2.*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n) + (4./3)*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n);
			}
			else if(this->boson == NC){
				hel_amps = -sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n)*(1-2*x) + 2.*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n)/sqrt(3.);
			}
		}
		break;
	case(n_1520):
		n=1;
	if(jz==3&&sign==minus){
		if(this->boson == EM){
			hel_amps = -sqrt(9./2)*this->model->T(W2, Q2, n);
		}
		else if(this->boson == CC){
			hel_amps = 3.*sqrt(2.)*this->model->T_minus(W2, q_mom, Q2, n);
		}
		else if(this->boson == NC){
			hel_amps = -sqrt(9./2)*(this->model->T_minus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n));
		}
	}
	else if(jz==1&&sign==minus){
		if(this->boson == EM){
			hel_amps = -sqrt(3./2)*this->model->T(W2, Q2, n) + sqrt(1./3)*this->model->lambda(W2, q_mom)*this->model->R(W2, q_mom, Q2, n);
		}
		else if(this->boson == CC){
			hel_amps = sqrt(6.)*this->model->T_minus(W2, q_mom, Q2, n) - (4./sqrt(3))*this->model->lambda(W2, q_mom)*this->model->R_minus(W2, q_mom, Q2, n);
		}
		else if(this->boson == NC){
			hel_amps = -sqrt(3./2)*(this->model->T_minus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n)) +
					sqrt(4./3)*this->model->lambda(W2, q_mom)*(this->model->R_minus(W2, q_mom, Q2, n) + x*this->model->R(W2, q_mom, Q2, n));
		}
	}
	else if(jz==1&&sign==plus){
		if (this->boson == EM){
			hel_amps = sqrt(3./2)*this->model->T(W2, Q2, n) - sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->R(W2, q_mom, Q2, n);
		}
		else if(this->boson == CC){
			hel_amps = sqrt(6.)*this->model->T_plus(W2, q_mom, Q2, n) - (4./3)*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->R_plus(W2, q_mom, Q2, n);
		}
		else if(this->boson == NC){
			hel_amps = sqrt(3./2)*(this->model->T_plus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n)) -
					sqrt(4./3)*this->model->lambda(W2, q_mom)*(this->model->R_plus(W2, q_mom, Q2, n) + 3*x*this->model->R(W2, q_mom, Q2, n));
		}
	}
	else if(jz==3&&sign==plus){
		if(this->boson == EM){
			hel_amps = -sqrt(9./2)*this->model->T(W2, Q2, n);
		}
		else if(this->boson == CC){
			hel_amps = 3.*sqrt(2.)*this->model->T_plus(W2, q_mom, Q2, n);
		}
		else if(this->boson == NC){
			hel_amps = -sqrt(9./2)*(this->model->T_plus(W2, q_mom, Q2, n) + 2.*x*this->model->T(W2, Q2, n));
		}
	}
	else if(jz==0&&sign==plus){
		if(this->boson == EM){
			hel_amps = sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n);
		}
		else if(this->boson == CC){
			hel_amps = -2.*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n) - (4./3)*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n);
		}
		else if(this->boson == NC){
			hel_amps = sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n)*(1-2*x) + 2.*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n)/sqrt(3.);
		}
	}
	else if(jz==0&&sign==minus){
		if(this->boson == EM){
			hel_amps = sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n);
		}
		else if(this->boson == CC){
			hel_amps = -2.*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n) + (4./3)*sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n);
		}
		else if(this->boson == NC){
			hel_amps = -sqrt(3.)*this->model->lambda(W2, q_mom)*this->model->S(W2, q_mom, Q2, n)*(1-2*x) - 2.*this->model->lambda(W2, q_mom)*this->model->C(W2, q_mom, Q2, n)/sqrt(3.);
		}
	}
		break;
	default:
		hel_amps = 0.;
		break;
	}

	return hel_amps;

}

std::vector<Flavour> Resonance_selector::get_resonances(int I){
	std::vector<Flavour> resonances;

	switch(this->res_charge){

		case(2):
			if(I==3){
				resonances.emplace_back(delta_pp);
			}
		break;
		case(1):
			if(I==1){
				resonances.emplace_back(p_1440);
				resonances.emplace_back(p_1520);
			}
			else if(I==3){
				resonances.emplace_back(delta_p);
			}
		break;
		case(0):
			if(I==1){
				resonances.emplace_back(n_1440);
				resonances.emplace_back(n_1520);
			}
			else if(I==3){
				resonances.emplace_back(delta_z);
			}
		break;
		case(-1):
			if(I==3){
				resonances.emplace_back(delta_m);
			}
	}

	return resonances;

}

std::vector<Flavour> Resonance_selector::get_resonances(){

	std::vector<Flavour> resonances;

	switch(this->res_charge){
		case(2):
			resonances.emplace_back(delta_pp);
		break;
		case(1):
			resonances.emplace_back(p_1440);
			resonances.emplace_back(p_1520);
			resonances.emplace_back(delta_p);
		break;
		case(0):
			resonances.emplace_back(n_1440);
			resonances.emplace_back(n_1520);
			resonances.emplace_back(delta_z);
		break;
		case(-1):
			resonances.emplace_back(delta_m);
		break;
	}
	return resonances;
}

double Resonance_selector::BW_2(Flavour& res, double W2){

	double BW_2 = res.get_width()/((2.*M_PI)*(pow(sqrt(W2) - res.get_mass(), 2) + pow(res.get_width()/2., 2)));

	return BW_2;
}

std::complex<double> Resonance_selector::BW(Flavour& res, double W2){

	std::complex<double> BW = sqrt(res.get_width()/(2.*M_PI))/(sqrt(W2) - res.get_mass() + 1i*res.get_width()/2.);

	return BW;
}
