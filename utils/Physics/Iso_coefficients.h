/*
 * Iso_coefficients.h
 *
 *  Created on: 25 Mar 2021
 *      Author: jack
 */

#ifndef ISO_COEFFICIENTS_H_
#define ISO_COEFFICIENTS_H_

#include "flavour.h"
#include <cmath>

namespace UTILS{

class Iso_coefficients{
private:
	Flavour *in_hadron, *out_hadron, *out_pion;
public:

	Iso_coefficients(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, const Flavour& out_pion);
	~Iso_coefficients();

	double get_coefficients(int I);

};

}



#endif /* ISO_COEFFICIENTS_H_ */
