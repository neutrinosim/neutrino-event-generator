/*
 * flavour.h
 *
 *  Created on: Nov 3, 2020
 *      Author: jack
 */

#ifndef FLAVOUR_H_
#define FLAVOUR_H_

#include <vector>
#include <stdio.h>
#include <type_traits>
#include <tuple>

namespace UTILS{

enum pdg_code{
	//leptons
	electron = 11, positron = -11,
	electron_nu = 12, electron_anu = -12,
	muon = 13, antimuon = -13,
	muon_nu = 14, muon_anu = -14,
	tau = 15, antitau = -15,
	tau_nu = 16, tau_anu = -16,

	//nucleons
	proton = 2212, neutron = 2112,

	//baryon resonances
	delta_pp = 2224, delta_p = 2214,
	delta_z = 2114, delta_m = 1114,

	p_1440 = 202212, n_1440 = 202112,
	p_1520 = 112214, n_1520 = 112114,

	//strange baryons
	lambda = 3122, sigma_z = 3212,
	sigma_p = 3222, sigma_m = 3112,

	//light mesons
	pion_p = 211, pion_m = -211,
	pion_z = 111,
};

class Flavour{

	pdg_code code;
	double mass, width, charge, mag_dipole;
	int I, J, L;

public:

	Flavour(pdg_code code);
	Flavour(long int int_code);
	Flavour(const Flavour& type);

	pdg_code get_code() const;

	int get_pdg_int() const;

	double get_mass() const;

	double get_width() const;

	double get_charge() const;

	double get_m_dipole() const;

	int get_I() const;

	int get_J() const;

	int get_L() const;

private:

	std::tuple<double, double, double, double, int, int, int> flavour_vals(pdg_code code);



};

}



#endif /* FLAVOUR_H_ */
