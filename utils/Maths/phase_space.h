 /*
 * Variable.h
 *
 *  Created on: 24 Sep 2020
 *      Author: jack
 */

#ifndef UTILS_MATHS_PHASE_SPACE_H_
#define UTILS_MATHS_PHASE_SPACE_H_

#include <random>
#include <iostream>
#include <vector>

namespace UTILS{

template <typename Scalar>
class Variable{
	//Variable limits
	Scalar x_min, x_max;
	//create PRNG
	std::mt19937_64 generator;
	std::uniform_real_distribution<Scalar> dist;
	//constructor
public:
	Variable(Scalar x_min, Scalar x_max):
		x_min(x_min), x_max(x_max){
		generator = std::mt19937_64();
		dist = std::uniform_real_distribution<Scalar>(x_min, x_max);
	}
	~Variable(){};
	//returns a random value on Variable
	virtual Scalar random_x(){
		Scalar x = dist(generator);
		return x;
	}
	Scalar xmin() const{return x_min;}

	Scalar xmax() const{return x_max;}
};

template <typename Scalar>
class Monochrom_Variable: public Variable<Scalar>{
	Scalar x;
public:
	Monochrom_Variable(Scalar x): Variable<Scalar>(x, x){
		this->x = x;
	}

	Scalar random_x(){
		return x;
	}
};

//struct Gaussian_Variable: public Variable{};

template <typename Scalar>
class Phase_space{

protected:

	int dim;
	std::vector<Variable<Scalar>> vars;

public:

	Phase_space(int dim,const std::vector<Variable<Scalar>> &vars){
		this->dim = dim;
		this->vars = vars;
	}
	Phase_space(const Variable<Scalar> &var){
		this->dim = 1;
		this->vars.push_back(var);
	}
	Phase_space(const Phase_space<Scalar> &P_space){
		this->dim = P_space.dim;
		this->vars = P_space.vars;
	}
	void add_variable(const Variable<Scalar> &var){
		this->dim += 1;
		this->vars.push_back(var);
	}
	void add_variables(std::vector<Variable<Scalar>> &vars){
		for(auto & var : vars){
			this->dim += 1;
			this->vars.push_back(var);
		}
	}
	std::vector<Scalar> get_point(){
		std::vector<Scalar> x;
		for(auto & var : vars){
			x.push_back(var.random_x());
		}
		return x;
	}

	int get_dim(){return dim;}

	Scalar volume(){
		Scalar V = 1;
		for(int i=0; i<dim;i++){
			if(vars[i].xmax() != vars[i].xmin()){
				V *= (vars[i].xmax() - vars[i].xmin());
			}
		}
		return V;
	}
};
}


#endif /* UTILS_MATHS_PHASE_SPACE_H_ */
