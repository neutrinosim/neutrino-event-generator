/*
 * 4Vec.h
 *
 *  Created on: 18 Sep 2020
 *      Author: jack
 *
 *  Defines a class for 4 Vectors with various usual functions
 */
#ifndef UTILS_MATHS_4VEC_H_
#define UTILS_MATHS_4VEC_H_

#include <cmath>

namespace UTILS {

	template<typename Scalar>class Vec4;
	template<typename Scalar>class Vec3;

	template<typename Scalar> class Vec4 {

	public:

		Scalar x[4];

		inline Vec4(){
			this->x[0]=this->x[1]=this->x[2]=this->x[3]=Scalar(0.0);
		}
		inline Vec4(const Vec4<Scalar>& vec4){
			this->x[0]=vec4[0];
			this->x[1]=vec4[1];
			this->x[2]=vec4[2];
			this->x[3]=vec4[3];
		}

		inline Vec4(const Scalar& x, const Vec3<Scalar>& vec){
			this->x[0]=x;
			this->x[1]=vec[0];
			this->x[2]=vec[1];
			this->x[3]=vec[2];
		}
		inline Vec4(const Scalar& x_0, const Scalar& x_1,
					const Scalar& x_2, const Scalar& x_3){
			this->x[0] = x_0;
			this->x[1] = x_1;
			this->x[2] = x_2;
			this->x[3] = x_3;

		}
		inline const Scalar operator[](int i) const{
			return x[i];
		}
		inline Scalar operator[](int i){
			return x[i];
		}
		inline Vec4<Scalar> operator+=(const Vec4<Scalar>& vec){
			x[0] += vec[0];
			x[1] += vec[1];
			x[2] += vec[2];
			x[3] += vec[3];
			return *this;
		}
		inline Vec4<Scalar> operator*=(const Scalar& alpha){
			x[0] *= alpha;
			x[1] *= alpha;
			x[2] *= alpha;
			x[3] *= alpha;
			return *this;
		}
		inline Vec4<Scalar> operator-=(const Vec4<Scalar>& vec){
			x[0] -= vec[0];
			x[1] -= vec[1];
			x[2] -= vec[2];
			x[3] -= vec[3];
			return *this;
		}
		template<typename Scalar2>
		inline Scalar operator*(const Vec4<Scalar2>& vec) const{
			return x[0]*vec[0] - x[1]*vec[1] - x[2]*vec[2] - x[3]*vec[3];
		}
		template<typename Scalar2>
		inline Scalar operator*(const Scalar2& alpha) const{
			return x[0]*alpha - x[1]*alpha - x[2]*alpha - x[3]*alpha;
		}
		template<typename Scalar2>
		inline Vec4<Scalar> operator+(const Vec4<Scalar2>& v) const{
			return Vec4(x[0]+v[0],x[1]+v[1],x[2]+v[2],x[3]+v[3]);
		}
		template<typename Scalar2>
		inline Vec4<Scalar> operator-(const Vec4<Scalar2>& v) const{
			return Vec4(x[0]-v[0],x[1]-v[1],x[2]-v[2],x[3]-v[3]);
		}
		template<typename Scalar2>
		inline Vec4<Scalar> operator=(const Vec4<Scalar2>& v) const{
			return Vec4(v[0],v[1],v[2],v[3]);
		}
		Vec4<Scalar> Lboost_z(const Scalar& gamma, const Scalar& beta){
			Scalar temp[4] = {x[0], x[1], x[2], x[3]};
			x[0] = gamma * (temp[0] - beta*temp[3]);
			x[3] = gamma * (temp[3] - beta*temp[0]);
			return *this;
		}
		Vec4<Scalar> Lboost(const Scalar& gamma, const Vec4<Scalar> v){
			Scalar temp[4] = {x[0], x[1], x[2], x[3]};
			x[0] = gamma*(temp[0] - v[1]*temp[1] - v[2]*temp[2] - v[3]*temp[3]);
			x[1] = temp[1] + v[1]*(-gamma*temp[0] + (gamma-1)*(v[1]*temp[1] + v[2]*temp[2] + v[3]*temp[3])/pow(v.get_3mag(),2));
			x[2] = temp[2] + v[2]*(-gamma*temp[0] + (gamma-1)*(v[1]*temp[1] + v[2]*temp[2] + v[3]*temp[3])/pow(v.get_3mag(),2));
			x[3] = temp[3] + v[3]*(-gamma*temp[0] + (gamma-1)*(v[1]*temp[1] + v[2]*temp[2] + v[3]*temp[3])/pow(v.get_3mag(),2));
			return *this;
		}
		Vec4<Scalar> Lrotate(const Scalar& theta, const Scalar& phi){
			Scalar temp[4] = {x[0], x[1], x[2], x[3]};
			x[0] = temp[0];
			x[1] = (temp[1]*cos(phi) + temp[2]*sin(phi))*cos(theta) - temp[3]*sin(theta);
			x[2] = -temp[1]*sin(phi) + temp[2]*cos(phi);
			x[3] = (temp[1]*cos(phi) + temp[2]*sin(phi))*sin(theta) + temp[3]*cos(theta);
			return *this;
		}
		Scalar get_3mag() const{
			Scalar mag;
			mag = std::sqrt(pow(x[1],2) + pow(x[2],2) + pow(x[3],2));
			return mag;
		}
		Scalar theta() const{
			Scalar theta = acos(x[3]/this->get_3mag());
			return theta;
		}
		Scalar phi() const{
			Scalar phi = atan(x[2]/x[1]);
			return phi;
		}
		Vec4<Scalar> invert(){
			x[1] = -x[1];
			x[2] = -x[2];
			x[3] = -x[3];
			return *this;
		}
	};


}
#endif /* UTILS_MATHS_4VEC_H_ */
