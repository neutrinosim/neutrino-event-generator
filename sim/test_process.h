/*
 * test_process.h
 *
 *  Created on: 9 Mar 2021
 *      Author: jack
 */

#ifndef TEST_PROCESS_H_
#define TEST_PROCESS_H_

#include "../utils/Physics/process.h"
#include "../utils/Physics/flavour.h"
#include "../utils/Physics/form_factor.h"
#include "../ext/result_writer.h"
#include <vector>

#include "me_2_test.h"

using namespace UTILS;

namespace SIM{

class test_process: public Process{

	UTILS::Flavour in_electron = UTILS::Flavour(11);
	UTILS::Flavour in_hadron = UTILS::Flavour(13);

	ME_2_test *me_2;

public:

	test_process(const Variable<double>& electron_spectrum, const Variable<double>& costheta): Process(in_electron, in_hadron, in_electron, electron_spectrum, costheta){

		me_2 = new ME_2_test(in_electron, in_hadron);

		std::vector<Variable<double>> addit_vars;
		addit_vars.emplace_back(0.0, 2*M_PI);
		this->p_space->add_variables(addit_vars);

	}

	virtual ~test_process(){};

	//Get random point in phase space (returns 4momentum)
	std::vector<Vec4<double>> get_particle_moms(){

		std::vector<double> point = this->p_space->get_point();

		double E = point[0];
		double cos_theta = point[1];
		double sin_theta = pow(1-pow(cos_theta,2), 0.5);
		double phi = point[2];

		//calculate incoming neutrino lepton 4mom
		Vec4<double> k_i = Vec4<double>(E, 0, 0, E);
		//calculate incoming hadron 4mom
		Vec4<double> p_i = Vec4<double>(E, 0, 0, -E);

		//calculate outgoing lepton 4mom
		Vec4<double> k_f = Vec4<double>(E,E*sin_theta*std::cos(phi),E*sin_theta*std::sin(phi), E*cos_theta);
		//calculate outgoing muon 4mom
		Vec4<double> p_f = Vec4<double>(E,-E*sin_theta*std::cos(phi),-E*sin_theta*std::sin(phi), -E*cos_theta);


		std::vector<Vec4<double>> moms;

		moms.push_back(k_i);
		moms.push_back(k_f);
		moms.push_back(p_i);
		moms.push_back(p_f);

		return moms;
	}

	double calc_diffcross(const std::vector<Vec4<double>> moms){

		double k = 1/(64*pow(M_PI,2)*((moms[0]+moms[2])*(moms[0]+moms[2])));
		double diff_cross = k*this->me_2->calc_ME_2(moms);

		return diff_cross;

	}
private:
	HepMC3::GenVertexPtr gen_interaction(const std::vector<Vec4<double>> moms){

		Vec4<double> k_i = moms[0];
		Vec4<double> k_f = moms[1];
		Vec4<double> p_i = moms[2];
		Vec4<double> p_f = moms[3];

		HepMC3::FourVector i_lep_mom = HepMC3::FourVector(k_i[1], k_i[2], k_i[3], k_i[0]);
		HepMC3::FourVector o_lep_mom = HepMC3::FourVector(k_f[1], k_f[2], k_f[3], k_f[0]);
		HepMC3::FourVector i_had_mom = HepMC3::FourVector(p_i[1], p_i[2], p_i[3], p_i[0]);
		HepMC3::FourVector o_had_mom = HepMC3::FourVector(p_f[1], p_f[2], p_f[3], p_f[0]);

		HepMC3::GenParticlePtr i_lep(new HepMC3::GenParticle(i_lep_mom, this->in_electron.get_pdg_int(), 4));
		HepMC3::GenParticlePtr o_lep(new HepMC3::GenParticle(o_lep_mom, this->in_electron.get_pdg_int(), 1));
		HepMC3::GenParticlePtr i_had(new HepMC3::GenParticle(i_had_mom, this->in_hadron.get_pdg_int(), 4));
		HepMC3::GenParticlePtr o_had(new HepMC3::GenParticle(o_had_mom, this->in_hadron.get_pdg_int(), 1));

		HepMC3::GenVertexPtr interaction(new HepMC3::GenVertex());

		interaction->add_particle_in(i_lep);
		interaction->add_particle_in(i_had);
		interaction->add_particle_out(o_lep);
		interaction->add_particle_out(o_had);

		return interaction;

	}

};
}




#endif /* TEST_PROCESS_H_ */
