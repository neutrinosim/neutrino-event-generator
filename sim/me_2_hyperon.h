/*
 * me_2_hyperon.h
 *
 *  Created on: 14 Mar 2021
 *      Author: jack
 */

#ifndef ME_2_HYPERON_H_
#define ME_2_HYPERON_H_

#include "../utils/Physics/me_2.h"
#include "../utils/Maths/vec4.h"
#include "../utils/Physics/flavour.h"

using namespace UTILS;

namespace SIM{

class ME_2_HYPERON: public ME_2{
private:

	UTILS::Form_Factor_Dipole *FV, *FM, *FA;
	UTILS::Double_Dipole *FP;

	Flavour *out_hyperon;

	double mk = 0.498; //kaon mass, serves as cutoff
	double m_pi = 0.135; //pion mass
	double m_p = 0.938; //proton mass
	double sin_cab = 0.225;

public:
	ME_2_HYPERON(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hyperon, double M_V, double M_A, double M_M);


	double const calc_ME_2(const std::vector<Vec4<double>> moms);

};

ME_2_HYPERON::ME_2_HYPERON(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hyperon, double M_V, double M_A, double M_M):
		ME_2(in_lepton, in_hadron, out_lepton){

	this->FV = new Form_Factor_Dipole(1.2247, M_V);
	this->FM = new Form_Factor_Dipole(1.793/(2*this->m_p), M_M);
	this->FA = new Form_Factor_Dipole(0.8793, M_A);
	this->FP = new Double_Dipole(0.8793*(in_hadron.get_mass()+out_hyperon.get_mass()), M_A, mk);

	this->out_hyperon = new Flavour(out_hyperon);

}

double const ME_2_HYPERON::calc_ME_2(const std::vector<Vec4<double>> moms){
	/*
	k_i = moms[0];
	k_f = moms[1];
	p_i = moms[2];
	p_f = moms[3];*/

	Vec4<double> q = moms[0] - moms[1];
	double Q2 = -1 * (q*q);
	double tau = Q2/(4*pow(this->in_hadron->get_mass(), 2));

	double FV = this->FV->calc_ff(Q2);
	double FM = this->FM->calc_ff(Q2);
	double FE = (FM*0.705)/(2*this->m_p*this->FM->F0);
	double FS = 0.344*FE;
	double FA = this->FA->calc_ff(Q2);
	double FP = this->FP->calc_ff(Q2);
	//ref: S.L. Mintz, L. Wen / Nuclear Physics A 766 (2006) 219–229
	double me_2 = 4*pow(FV,2)*((moms[1]*moms[3])*(moms[0]*moms[2]) + (moms[1]*moms[2])*(moms[0]*moms[3])
			- (moms[1]*moms[0])*this->in_hadron->get_mass()*this->out_hyperon->get_mass());
	me_2 += 4*pow(FA,2)*((moms[1]*moms[3])*(moms[0]*moms[2]) + (moms[1]*moms[2])*(moms[0]*moms[3])
			+ (moms[0]*moms[1])*this->in_hadron->get_mass()*this->out_hyperon->get_mass());
	me_2 += 2*(pow(FP,2)/pow(m_pi,2))*( (moms[1]*moms[1])*(moms[1]*moms[0])*( moms[0]*moms[2] + moms[1]*moms[2] -
			this->in_hadron->get_mass()*(this->out_hyperon->get_mass() - this->in_hadron->get_mass())) );
	me_2 += (pow(FS,2)/(2*pow(m_p,2)))*( (moms[1]*moms[1])*(moms[1]*moms[0])*( moms[0]*moms[2] + moms[1]*moms[2] +
			this->in_hadron->get_mass()*(this->out_hyperon->get_mass() + this->in_hadron->get_mass())) );
	me_2 -= 8*FV*FA*( (moms[1]*moms[0])*(moms[1]*moms[2] + moms[0]*moms[2]) - (moms[1]*moms[1])*(moms[1]*moms[2]) );
	me_2 += (FM*FE/pow(m_p,2))*((moms[1]*moms[0] + moms[1]*moms[2] - moms[1]*moms[1])*(moms[0]*moms[2])*(moms[1]*moms[1]));
	me_2 += 2*(FV*FS/m_p)*((moms[1]*moms[1])*(moms[0]*moms[2])*(this->in_hadron->get_mass()+this->out_hyperon->get_mass())
			- (moms[1]*moms[0])*this->in_hadron->get_mass());
	me_2 -= 4*(FA*FP/m_pi)*( (moms[1]*moms[1])*((moms[0]*moms[2])*(this->out_hyperon->get_mass()-this->in_hadron->get_mass())
			+ (moms[1]*moms[0])*this->in_hadron->get_mass()) );
	me_2 -= 4*(FA*FM/m_p) * ((moms[1]*moms[0])*(this->in_hadron->get_mass()*(moms[1]*moms[3] + moms[0]*moms[3]) + this->out_hyperon->get_mass()*(moms[1]*moms[2] + moms[0]*moms[2]))
			- (moms[1]*moms[1])*(this->in_hadron->get_mass()*(moms[0]*moms[3]) + this->out_hyperon->get_mass()*(moms[0]*moms[2])));
	me_2 += 4*(FV*FM/m_p) * ( (moms[1]*moms[0])*(moms[0]*moms[2] - moms[1]*moms[2])*(this->out_hyperon->get_mass()-this->in_hadron->get_mass())
			+ 2*pow(moms[1]*moms[0],2)*this->in_hadron->get_mass() -
			(moms[1]*moms[1])*(0.5*(moms[0]*moms[2])*(this->out_hyperon->get_mass()-this->in_hadron->get_mass()) + 1.5*this->in_hadron->get_mass()*(moms[1]*moms[0]) ));
	me_2 += pow(FM/m_p,2)*( 2*(moms[1]*moms[0])*((moms[1]*moms[2])*(moms[1]*moms[3]) + (moms[0]*moms[2])*(moms[0]*moms[3]) + (moms[1]*moms[0])*this->in_hadron->get_mass()*this->out_hyperon->get_mass())
			- (moms[1]*moms[1])*(2*(moms[0]*moms[2])*(moms[0]*moms[3]) + 0.5*(moms[1]*moms[0])*(moms[2]*moms[3]) + 1.5*(moms[1]*moms[0])*this->out_hyperon->get_mass()*this->in_hadron->get_mass()));
	me_2 += 4*(FE*FV/m_p)*(this->in_hadron->get_mass()*(moms[1]*moms[0]*(moms[1]*moms[3] + moms[0]*moms[3]) - (moms[1]*moms[1])*(moms[0]*moms[3]))
			- this->out_hyperon->get_mass()*(moms[1]*moms[0]*(moms[1]*moms[2] + moms[0]*moms[2]) - (moms[1]*moms[1])*(moms[0]*moms[2])));
	me_2 += pow(FE/m_p,2)*( 2*(moms[1]*moms[0])*((moms[1]*moms[2])*(moms[1]*moms[3]) + (moms[0]*moms[2])*(moms[0]*moms[3]) - (moms[1]*moms[0])*this->out_hyperon->get_mass()*this->in_hadron->get_mass())
			-(moms[1]*moms[1])*(2*(moms[0]*moms[2])*(moms[0]*moms[3]) + 0.5*(moms[1]*moms[0])*(moms[2]*moms[3]) - 1.5*(moms[1]*moms[0])*this->out_hyperon->get_mass()*this->in_hadron->get_mass()));
	me_2 += 4*(FA*FE/m_p)*(this->in_hadron->get_mass()*((moms[1]*moms[0])*(moms[1]*moms[3] - moms[0]*moms[3]) + 0.5*(moms[0]*moms[3])*(moms[1]*moms[1]))
			+ this->out_hyperon->get_mass()*((moms[1]*moms[0])*(moms[1]*moms[2] - moms[0]*moms[2]) + 0.5*(moms[0]*moms[2])*(moms[1]*moms[1])));
	me_2 *= (pow(G_F*sin_cab, 2))/(2*this->in_hadron->get_mass()*this->out_hyperon->get_mass());

	return me_2;

}

}

#endif /* ME_2_HYPERON_H_ */
