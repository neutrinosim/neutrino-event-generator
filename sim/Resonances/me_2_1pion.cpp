/*
 * me_2_1pion.cpp
 *
 *  Created on: 23 Mar 2021
 *      Author: jack
 */

#include "me_2_1pion.h"

using namespace SIM;
using namespace UTILS;

ME_2_1pion::ME_2_1pion(bool coh, process_type boson, const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, const Flavour& out_pion, double M_V, double M_A):
	ME_2(in_lepton, in_hadron, out_lepton){

	int res_charge = out_hadron.get_charge() + out_pion.get_charge();

	this->CG = new Iso_coefficients(in_lepton, in_hadron, out_lepton, out_hadron, out_pion);

	this->res_sel = new Resonance_selector(boson, in_hadron, res_charge, M_V, M_A);

	this->out_hadron = new Flavour(out_hadron);
	this->out_pion = new Flavour(out_pion);

	this->boson = boson;

	this->coh = coh;

}

ME_2_1pion::~ME_2_1pion(){};

double const ME_2_1pion::calc_ME_2(const std::vector<Vec4<double>> moms){

	double me_2 = 0.;
	double W2 = moms[3]*moms[3];

	if(W2 < pow(this->out_hadron->get_mass() + this->out_pion->get_mass(), 2)){
		me_2 = 0.;
	}else{

		//need to boost 4momenta into rest frame of resonant particle
		double gamma = moms[3][0]/sqrt(W2);
		Vec4<double> k_i = moms[0];
		Vec4<double> k_f = moms[1];

		Vec4<double> p_f = moms[3];

		Vec4<double> v = p_f;
		v *= 1/moms[3][0];

		k_i.Lboost(gamma, v);
		k_f.Lboost(gamma, v);
		p_f.Lboost(gamma, v);

		Vec4<double> q = k_i - k_f;
		double Q2 = abs(q*q);
		double q_mom = q.get_3mag();

		std::vector<double> hel_amps_plus, hel_amps_minus;
		if(this->coh == false){ //incoherent scattering

			double A_2=0;
			vector<Flavour> resonances_3 = this->res_sel->get_resonances(3);
			vector<Flavour> resonances_1 = this->res_sel->get_resonances(1);
			for(int jz = 1; jz<=3; jz+=2){
				p_m sign = UTILS::plus;
				for(auto r : resonances_3){
					A_2 += pow(this->res_sel->hel_amps(r, abs(jz), sign, W2, q_mom, Q2)*this->CG->get_coefficients(r.get_I()),2)*res_sel->BW_2(r, W2)*pow(x_e(r),2);
				}
				for(auto r : resonances_1){
					A_2 += pow(this->res_sel->hel_amps(r, abs(jz), sign, W2, q_mom, Q2)*this->CG->get_coefficients(r.get_I()),2)*res_sel->BW_2(r, W2)*pow(x_e(r),2);
				}
			}
			me_2 = pow(k_f[0] + k_i[0] - q_mom, 2)*A_2;
			A_2 = 0;
			for(int jz = 1; jz<=3; jz+=2){
				p_m sign = UTILS::minus;
				for(auto r : resonances_3){
					A_2 += pow(this->res_sel->hel_amps(r, abs(jz), sign, W2, q_mom, Q2)*this->CG->get_coefficients(r.get_I()),2)*res_sel->BW_2(r, W2)*pow(x_e(r),2);
				}
				for(auto r : resonances_1){
					A_2 += pow(this->res_sel->hel_amps(r, abs(jz), sign, W2, q_mom, Q2)*this->CG->get_coefficients(r.get_I()),2)*res_sel->BW_2(r, W2)*pow(x_e(r),2);
				}
			}
			me_2 += pow(k_f[0] + k_i[0] + q_mom, 2)*A_2;
			me_2 *= Q2/pow(q_mom,2);
			A_2 = 0;
			//generate scalar helicity amplitudes
			for(auto r : resonances_3){
				A_2 += pow(this->res_sel->hel_amps(r, 0, UTILS::plus, W2, q_mom, Q2)*this->CG->get_coefficients(r.get_I()),2)*res_sel->BW_2(r, W2)*pow(x_e(r),2);
			}
			for(auto r : resonances_1){
				A_2 += pow(this->res_sel->hel_amps(r, 0, UTILS::minus, W2, q_mom, Q2)*this->CG->get_coefficients(r.get_I()),2)*res_sel->BW_2(r, W2)*pow(x_e(r),2);
			}
			me_2 += 2.*(pow(this->in_hadron->get_mass(),2)/W2)*(4.*k_f[0]*k_i[0] - Q2)*A_2;
			me_2 *= 2.*pow(this->in_hadron->get_mass(),2)*pow(this->c.G_F, 2);

		}
		else{ //coherent scattering

			vector<Flavour> resonances = this->res_sel->get_resonances();

			std::complex<double> me_a=0., me_b=0., me_c=0., me_d=0.; //+1/2, -1/2

			Vec4<double> p_pi = moms[4];
			p_pi.Lboost(gamma, v);
			p_pi.Lrotate(v.theta(), v.phi());

			for(auto r : resonances){
				me_a += sqrt(Q2/pow(q_mom,2))*( (k_f[0] + k_i[0] + q_mom)*(CG_J(r.get_J(),3, 1)*this->res_sel->hel_amps(r, 3, UTILS::minus, W2, q_mom, Q2)*d_func(r.get_J(), 3, 1, p_pi.theta())*exp(1i*p_pi.phi())) -
						(k_f[0] + k_i[0] - q_mom)*(CG_J(r.get_J(),-1, 1)*this->res_sel->hel_amps(r, 1, UTILS::plus, W2, q_mom, Q2)*d_func(r.get_J(), -1, 1, p_pi.theta())*exp(-1i*p_pi.phi())) ) +
						(this->in_hadron->get_mass()/sqrt(W2))*sqrt(2.*(4.*k_f[0]*k_i[0] - Q2))*(CG_J(r.get_J(),1, 1)*this->res_sel->hel_amps(r, 0, UTILS::plus, W2, q_mom, Q2)*d_func(r.get_J(), 1, 1, p_pi.theta()));
				me_a *= ((2*r.get_J()+1)/sqrt(2))*get_decay_sign(r)*x_e(r)*this->CG->get_coefficients(r.get_I())*res_sel->BW(r, W2);

				me_b += sqrt(Q2/pow(q_mom,2))*( (k_f[0] + k_i[0] + q_mom)*(CG_J(r.get_J(),-3, 1)*this->res_sel->hel_amps(r, 3, UTILS::plus, W2, q_mom, Q2)*d_func(r.get_J(), -3, 1, p_pi.theta())) -
						(k_f[0] + k_i[0] - q_mom)*(CG_J(r.get_J(),1, 1)*this->res_sel->hel_amps(r, 1, UTILS::minus, W2, q_mom, Q2)*d_func(r.get_J(), 1, 1, p_pi.theta())*exp(2.*1i*p_pi.phi()))) +
						(this->in_hadron->get_mass()/sqrt(W2))*sqrt(2.*(4.*k_f[0]*k_i[0] - Q2))*(CG_J(r.get_J(),-1, 1)*this->res_sel->hel_amps(r, 0, UTILS::minus, W2, q_mom, Q2)*d_func(r.get_J(), -1, 1, p_pi.theta())*exp(1i*p_pi.phi()));
				me_b *= ((2*r.get_J()+1)/sqrt(2))*get_decay_sign(r)*x_e(r)*this->CG->get_coefficients(r.get_I())*res_sel->BW(r, W2);

				me_c += sqrt(Q2/pow(q_mom,2))*( (k_f[0] + k_i[0] + q_mom)*(CG_J(r.get_J(),3, -1)*this->res_sel->hel_amps(r, 3, UTILS::minus, W2, q_mom, Q2)*d_func(r.get_J(), 3, -1, p_pi.theta())*exp(1i*p_pi.phi())) -
						(k_f[0] + k_i[0] - q_mom)*(CG_J(r.get_J(),-1, -1)*this->res_sel->hel_amps(r, 1, UTILS::plus, W2, q_mom, Q2)*d_func(r.get_J(), -1, -1, p_pi.theta())*exp(-2.*1i*p_pi.phi())) ) +
						(this->in_hadron->get_mass()/sqrt(W2))*sqrt(2.*(4.*k_f[0]*k_i[0] - Q2))*(CG_J(r.get_J(),1, -1)*this->res_sel->hel_amps(r, 0, UTILS::plus, W2, q_mom, Q2)*d_func(r.get_J(), 1, -1, p_pi.theta())*exp(-1i*p_pi.phi()));
				me_c *= ((2*r.get_J()+1)/sqrt(2))*get_decay_sign(r)*x_e(r)*this->CG->get_coefficients(r.get_I())*res_sel->BW(r, W2);

				me_d += sqrt(Q2/pow(q_mom,2))*( (k_f[0] + k_i[0] + q_mom)*(CG_J(r.get_J(),-3, -1)*this->res_sel->hel_amps(r, 3, UTILS::plus, W2, q_mom, Q2)*d_func(r.get_J(), -3, -1, p_pi.theta())*exp(-1i*p_pi.phi())) -
						(k_f[0] + k_i[0] - q_mom)*(CG_J(r.get_J(),1, -1)*this->res_sel->hel_amps(r, 1, UTILS::minus, W2, q_mom, Q2)*d_func(r.get_J(), 1, -1, p_pi.theta())*exp(1i*p_pi.phi())) ) +
						(this->in_hadron->get_mass()/sqrt(W2))*sqrt(2.*(4.*k_f[0]*k_i[0] - Q2))*(CG_J(r.get_J(),-1, -1)*this->res_sel->hel_amps(r, 0, UTILS::minus, W2, q_mom, Q2)*d_func(r.get_J(), -1, -1, p_pi.theta()));
				me_d *= ((2*r.get_J()+1)/sqrt(2))*get_decay_sign(r)*x_e(r)*this->CG->get_coefficients(r.get_I())*res_sel->BW(r, W2);
			}

			me_2 = 8*pow(this->in_hadron->get_mass(),2) * pow(moms[0][0],2) *
					(pow(std::abs(me_a),2) + pow(std::abs(me_b),2) + pow(std::abs(me_c),2) + pow(std::abs(me_d),2));

	}
	if(isnan(me_2)){
		me_2 = 0.;
	}

	if(this->boson == CC){
		me_2 *= pow(this->c.G_F*this->c.Vud, 2)/2;
	}else if(this->boson == NC){
		me_2 *= pow(this->c.G_F, 2)/2;
	}else if(this->boson == EM){
		me_2 *= pow(4*M_PI/137,2);
	}
}
	return me_2;
}

double ME_2_1pion::get_decay_sign(Flavour& res){
	double sgn;

	switch(res.get_code()){
	case(delta_pp):
	case(delta_p):
	case(delta_z):
	case(delta_m):
		sgn = +1.;
		break;
	case(p_1440):
	case(n_1440):
		sgn = +1.;
		break;
	case(p_1520):
	case(n_1520):
		sgn = -1.;
		break;
	default:
		sgn = 0.;
		break;
	}
	return sgn;
}

double ME_2_1pion::x_e(Flavour& res){
	double x_e;

	switch(res.get_code()){
	case(delta_pp):
	case(delta_p):
	case(delta_z):
	case(delta_m):
		x_e = +1.;
		break;

	case(p_1440):
	case(n_1440):
		x_e = .6;
		break;
	case(p_1520):
	case(n_1520):
		x_e = .6;
		break;
	default:
		x_e = 0.;
		break;
	}
	return x_e;
}

double ME_2_1pion::d_func(int J, int j_i, int j_f, double theta){

	double d=0.;

	if(J==3){
		if(j_i==3 && j_f==1){
			d= -sqrt(3.)*(1+cos(theta))*cos(theta/2)/2;
		}else
		if(j_i==3 && j_f==-1){
			d= sqrt(3.)*(1-cos(theta))*cos(theta/2)/2;
		}else
		if(j_i==1 && j_f==1){
			d= (3*cos(theta)-1)*cos(theta/2)/2;
		}else
		if(j_i==1 && j_f==-1){
			d= -(3*cos(theta)+1)*sin(theta/2)/2;
		}
		if(j_i==-3 && j_f==-1){
			d= sqrt(3.)*(1+cos(theta))*cos(theta/2)/2;
		}else
		if(j_i==-3 && j_f==1){
			d= -sqrt(3.)*(1-cos(theta))*cos(theta/2)/2;
		}else
		if(j_i==-1 && j_f==-1){
			d= (3*cos(theta)-1)*cos(theta/2)/2;
		}else
		if(j_i==-1 && j_f==1){
			d= -(3*cos(theta)+1)*sin(theta/2)/2;
		}
	}else
	if(J==1){
		if(j_i==1 && j_f==1){
			d= cos(theta/2);
		}else
		if(j_i==1 && j_f==-1){
			d= -sin(theta/2);
		}else
		if(j_i==-1 && j_f==-1){
			d= cos(theta/2);
		}else
		if(j_i==-1 && j_f==1){
			d= -sin(theta/2);
		}
	}

	return d;
}

double ME_2_1pion::CG_J(int J, int jz, int s){

	double cg_j = 0.;

	if(J==3&&jz==3&&s==1){
		cg_j = 1.;
	}else
	if(J==3&&jz==1&&s==-1){
		cg_j = sqrt(1./3);
	}else
	if(J==3&&jz==1&&s==1){
		cg_j = sqrt(2./3);
	}else
	if(J==3&&jz==-1&&s==-1){
		cg_j = sqrt(2./3);
	}else
	if(J==3&&jz==-1&&s==1){
		cg_j = sqrt(1./3);
	}else
	if(J==3&&jz==-3&&s==-1){
		cg_j = 1.;
	}if(J==1&&jz==1&&s==-1){
		cg_j = sqrt(2./3);
	}else
	if(J==1&&jz==1&&s==1){
		cg_j = -sqrt(1./3);
	}else
	if(J==1&&jz==-1&&s==-1){
		cg_j = sqrt(1./3);
	}else
	if(J==1&&jz==-1&&s==1){
		cg_j = -sqrt(2./3);
	}

	return cg_j;

}
