/*
 * resonances.cpp
 *
 *  Created on: 17 Feb 2021
 *      Author: jack
 */

#include "resonances.h"

using namespace SIM;
using namespace UTILS;

Resonance::Resonance(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton,const Flavour& out_hadron, const Flavour& out_pion, bool coh,process_type boson,
		const Variable<double>& neutrino_spectrum, const Variable<double>& costheta, double M_V, double M_A):
	Process(in_lepton, in_hadron, out_lepton, neutrino_spectrum, costheta){

	this->out_hadron = new Flavour(out_hadron);
	this->out_pion = new Flavour(out_pion);

	//variables are:  phi, final lepton energy fraction
	std::vector<Variable<double>> addit_vars;
	addit_vars.emplace_back(0.0, 2*M_PI);
	addit_vars.emplace_back(0., 1.);
	addit_vars.emplace_back(-1., 1.);
	addit_vars.emplace_back(0.0, 2*M_PI);
	this->p_space->add_variables(addit_vars);

	this->me_2 = new ME_2_1pion(coh, boson, in_lepton, in_hadron, out_lepton, out_hadron, out_pion, M_V, M_A);

}

std::vector<Vec4<double>> Resonance::get_particle_moms(){
	double E_neutrino, p_neutrino, E_lepton, p_lepton, cos_theta, sin_theta, phi;
	std::vector<double> point = this->p_space->get_point();

	E_neutrino = point[0];
	cos_theta = point[1];
	sin_theta = pow(1-pow(cos_theta,2), 0.5);
	phi = point[2];
	double x = point[3];

	p_neutrino = E_neutrino;
	E_lepton = x*E_neutrino;

	p_lepton = sqrt(pow(E_lepton, 2) - pow(this->out_lepton->get_mass(),2));

	Vec4<double> k_i, k_f, p_i;
	//calculate incoming neutrino and outgoing lepton 4mom
	k_i = Vec4<double>(E_neutrino, 0, 0, p_neutrino);
	k_f = Vec4<double>(E_lepton,p_lepton*sin_theta*std::cos(phi),p_lepton*sin_theta*std::sin(phi), p_lepton*cos_theta);
	//calculate incoming/outgoing hadron 4mom
	p_i = Vec4<double>(this->in_hadron->get_mass(), 0, 0, 0);
	Vec4<double> p_f = (k_i + p_i) - k_f;
	std::vector<Vec4<double>> moms;

	moms.push_back(k_i);
	moms.push_back(k_f);
	moms.push_back(p_i);
	moms.push_back(p_f);

	//need to generate pion/final hadron in resonance rest frame then boost back
	double cos_theta_pi = point[4];
	double sin_theta_pi = sqrt(1-pow(cos_theta_pi,2));
	double phi_pi = point[5];

	double W = sqrt(p_f*p_f);

	double pi_mom = sqrt(pow(W,4) -2*pow(W,2)*(pow(this->out_hadron->get_mass(),2) + pow(this->out_pion->get_mass(),2)) +
			pow(pow(this->out_hadron->get_mass(),2) - pow(this->out_pion->get_mass(),2),2))/(2*W);
	double E_pi = sqrt(pow(pi_mom,2) + pow(this->out_pion->get_mass(),2));

	Vec4<double> p_pi(E_pi, pi_mom*sin_theta_pi*cos(phi_pi), pi_mom*sin_theta_pi*sin(phi_pi), pi_mom*cos_theta_pi);
	p_pi.Lrotate(p_f.theta(),p_f.phi()); //rotate to align with p_f
	double E_had = sqrt(pow(pi_mom,2) + pow(this->out_hadron->get_mass(),2));

	Vec4<double> p_had(E_had, -pi_mom*sin_theta_pi*cos(phi_pi), -pi_mom*sin_theta_pi*sin(phi_pi), -pi_mom*cos_theta_pi);
	p_had.Lrotate(p_f.theta(),p_f.phi());

	double gamma = moms[3][0]/W;
	Vec4<double> v = p_f;
	v *= 1/moms[3][0];
	v.invert();

	p_pi.Lboost(gamma, v);
	p_had.Lboost(gamma, v);

	moms.push_back(p_pi);
	moms.push_back(p_had);

	return moms;

}

double Resonance::calc_diffcross(const std::vector<Vec4<double>> moms){
	double diff_cross;
	double W = sqrt(moms[3]*moms[3]);

	Vec4<double> v = moms[3];
	v *= 1/moms[3][0];

	Vec4<double> k_i = moms[0];
	Vec4<double> k_f = moms[1];

	double gamma = moms[3][0]/W;

	k_i.Lboost(gamma, v);
	k_f.Lboost(gamma, v);

	Vec4<double> q = moms[1] - moms[0];
	double pi_mom = sqrt(pow(W,4) -2*pow(W,2)*(pow(this->out_hadron->get_mass(),2) + pow(this->out_pion->get_mass(),2)) +
				pow(pow(this->out_hadron->get_mass(),2) - pow(this->out_pion->get_mass(),2),2))/(2*W);

	if(moms[1][0]<this->out_lepton->get_mass()){
		diff_cross = 0;
	}else{
		diff_cross = this->me_2->calc_ME_2(moms);
		diff_cross *= pi_mom*(-1*(q*q))/(4*pow(q.get_3mag(), 2)*pow(2*M_PI, -4));
		diff_cross *= this->in_hadron->get_mass()*moms[0][0]*moms[1][0]/(M_PI*W);
	}

	return diff_cross;
}

HepMC3::GenVertexPtr Resonance::gen_interaction(const std::vector<Vec4<double>> moms){

	Vec4<double> k_i = moms[0];
	Vec4<double> k_f = moms[1];
	Vec4<double> p_i = moms[2];
	Vec4<double> p_f = moms[3];
	Vec4<double> p_pi = moms[4];
	Vec4<double> p_had = moms[5];

	HepMC3::FourVector i_lep_mom = HepMC3::FourVector(k_i[1], k_i[2], k_i[3], k_i[0]);
	HepMC3::FourVector o_lep_mom = HepMC3::FourVector(k_f[1], k_f[2], k_f[3], k_f[0]);
	HepMC3::FourVector i_had_mom = HepMC3::FourVector(p_i[1], p_i[2], p_i[3], p_i[0]);

	HepMC3::FourVector o_pi_mom = HepMC3::FourVector(p_pi[1], p_pi[2], p_pi[3], p_pi[0]);
	HepMC3::FourVector o_had_mom = HepMC3::FourVector(p_had[1], p_had[2], p_had[3], p_had[0]);

	HepMC3::GenParticlePtr i_lep(new HepMC3::GenParticle(i_lep_mom, this->in_neutrino->get_pdg_int(), 4));
	HepMC3::GenParticlePtr o_lep(new HepMC3::GenParticle(o_lep_mom, this->out_lepton->get_pdg_int(), 1));
	HepMC3::GenParticlePtr i_had(new HepMC3::GenParticle(i_had_mom, this->in_hadron->get_pdg_int(), 4));
	HepMC3::GenParticlePtr o_pi(new HepMC3::GenParticle(o_pi_mom, this->out_pion->get_pdg_int(), 1));
	HepMC3::GenParticlePtr o_had(new HepMC3::GenParticle(o_had_mom, this->out_hadron->get_pdg_int(), 1));

	HepMC3::GenVertexPtr interaction(new HepMC3::GenVertex());

	interaction->add_particle_in(i_lep);
	interaction->add_particle_in(i_had);
	interaction->add_particle_out(o_lep);
	interaction->add_particle_out(o_pi);
	interaction->add_particle_out(o_had);

	return interaction;

}


