/*
 * resonances.h
 *
 *  Created on: 17 Feb 2021
 *      Author: jack
 */

#ifndef RESONANCES_H_
#define RESONANCES_H_

#include "../../utils/Physics/process.h"
#include "../../utils/Maths/phase_space.h"
#include "../../utils/Physics/me_2.h"
#include "../../utils/Physics/process_type.h"

#include "me_2_1pion.h"

#include <vector>

using namespace UTILS;

namespace SIM{

class Resonance: public Process{

private:

	//Variable to generate final lepton energy

	Variable<double> lep_energy = Variable<double>(0.0, 1.);

	//outgoing particles

	Flavour *out_hadron, *out_pion;

	//ME_2
	ME_2 *me_2;

public:

	Resonance(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton,const Flavour& out_hadron, const Flavour& out_pion, bool coh, process_type boson,
			const Variable<double>& neutrino_spectrum, const Variable<double>& costheta, double M_V, double M_A);

	std::vector<Vec4<double>> get_particle_moms();

	//Calculates differential cross-section for a point in phase space
	double calc_diffcross(const std::vector<Vec4<double>> moms);

	//Generating interaction vertex
	HepMC3::GenVertexPtr gen_interaction(const std::vector<Vec4<double>> moms);

};




}



#endif /* RESONANCES_H_ */
