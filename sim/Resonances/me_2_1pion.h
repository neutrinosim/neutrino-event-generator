/*
 * me_2_1pion.h
 *
 *  Created on: 23 Mar 2021
 *      Author: jack
 */

#ifndef RESONANCES_ME_2_1PION_H_
#define RESONANCES_ME_2_1PION_H_

#include "../../utils/Physics/me_2.h"
#include "../../utils/Physics/resonance_selector.h"
#include "../../utils/Physics/physics_constants.h"
#include "../../utils/Physics/Iso_coefficients.h"

#include <vector>
#include <complex>

using namespace UTILS;

namespace SIM{

class ME_2_1pion: public ME_2{
private:

	constants c = constants();
	Iso_coefficients *CG;

	Resonance_selector *res_sel;

	Flavour *out_hadron, *out_pion;

	process_type boson;

	bool coh;

public:

	ME_2_1pion(bool coh, process_type boson, const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, const Flavour& out_pion, double M_V, double M_A);

	~ME_2_1pion();

	double const calc_ME_2(const std::vector<Vec4<double>> moms);

	double get_decay_sign(Flavour& res);

	double x_e(Flavour& res); //elasticity

	double d_func(int J, int j_i, int j_f, double theta);

	double CG_J(int J, int l, int jz);

};

}

#endif /* RESONANCES_ME_2_1PION_H_ */
