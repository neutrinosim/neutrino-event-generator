/*
 * me_2_emag.h
 *
 *  Created on: 8 Feb 2021
 *      Author: jack
 */

#ifndef ME_2_EMAG_H_
#define ME_2_EMAG_H_

#include "../utils/Physics/me_2.h"

using namespace UTILS;

namespace SIM{

class ME_2_emag: public ME_2{
private:
	double M_V;
	Flavour *out_hadron;

	UTILS::Form_Factor *GD;

public:

	ME_2_emag(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, double M_V): ME_2(in_lepton, in_hadron, out_lepton){

		this->M_V = M_V;

		this->GD = new UTILS::Form_Factor_Dipole(1., M_V);

		this->out_hadron = new Flavour(out_hadron);
	}

	~ME_2_emag(){}

	double const calc_ME_2(const std::vector<Vec4<double>> moms){

		Vec4<double> k_i = moms[0];
		Vec4<double> k_f = moms[1];
		Vec4<double> p_i = moms[2];
		Vec4<double> p_f = moms[3];

		Vec4<double> q = k_i - k_f;
		double Q2 = -1 * (q*q);
		double tau = Q2/(4*pow(this->in_hadron->get_mass(), 2));

		double GD = this->GD->calc_ff(Q2);

		double GE = this->in_hadron->get_charge()*GD;
		//GM = this->in_hadron->get_m_dipole()*GD;
		double GM = 0;

		double W2 = (pow(GE,2) + tau*pow(GM,2))/(1+tau);

		double me_2 = W2*(2.*(k_f*p_i)*(k_i*p_i) - (p_i*p_i)*(k_f*k_i)) - 2.*tau*pow(GM,2)*(k_f*k_i); //ref: Electron Scattering for Nuclear and Nucleon Structure - Walecka
		me_2 *= 8./pow(Q2,2);

		return me_2;
	}

};
}

#endif /* ME_2_EMAG_H_ */
