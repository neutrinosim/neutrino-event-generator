/*
 * Bodek_Form_factor.h
 *
 *  Created on: 22 Mar 2021
 *      Author: jack
 */

#ifndef BRADFORD07_FORM_FACTOR_H_
#define BRADFORD07_FORM_FACTOR_H_

#include "../utils/Physics/form_factor.h"

namespace SIM{

class Bradford07_Form_Factor: public UTILS::Form_Factor{

public:

	Bradford07_Form_Factor();

	~Bradford07_Form_Factor();



};

}
#endif /* BRADFORD07_FORM_FACTOR_H_ */
