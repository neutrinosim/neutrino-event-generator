/*
 * me_2_qecc.h
 *
 *  Created on: Nov 11, 2020
 *      Author: jack
 */

#ifndef ME_2_QECC_H_
#define ME_2_QECC_H_

#include "../utils/Physics/me_2.h"
#include "../utils/Physics/flavour.h"
#include "../utils/Physics/form_factor.h"

using namespace UTILS;

namespace SIM{

class ME_2_QECC: protected UTILS::ME_2{
private:

	double M_V_i, M_V_f, M_A;
	double M_pion = 0.13957; //GeV

	Flavour *out_hadron;

	UTILS::Form_Factor_Dipole *GD_i, *GD_f, *FA;

	const double g_A = -1.267;//ref: Leitner2005

public:
	ME_2_QECC(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, double M_V_i, double M_V_f, double M_A);

	~ME_2_QECC(){};

	double const calc_ME_2(const std::vector<Vec4<double>> moms);


};

ME_2_QECC::ME_2_QECC(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, double M_V_i, double M_V_f, double M_A):
	ME_2(in_neutrino, in_hadron, out_lepton){

	this->M_V_i = M_V_i;
	this->M_V_f = M_V_f;
	this->M_A = M_A;

	this->GD_i = new UTILS::Form_Factor_Dipole(1., M_V_i);
	this->GD_f = new UTILS::Form_Factor_Dipole(1., M_V_f);
	this->FA = new UTILS::Form_Factor_Dipole(g_A, M_A);

	this->out_hadron = new Flavour(out_hadron);
}

double const ME_2_QECC::calc_ME_2(const std::vector<Vec4<double>> moms){

	Vec4<double> q = moms[0] - moms[1];
	double Q2 = -1 * (q*q);
	double tau = Q2/(4*pow(this->in_hadron->get_mass(), 2));

	//Define combined kinematic factors
	double K1, K2, K3, K4, K5;

	K1 = pow(this->in_hadron->get_mass(),2) * (moms[0] * moms[1]);

	K2 = (moms[0] * moms[2]) * (moms[1] * moms[2]) - K1/2;

	K3 = (moms[0] * moms[2])*(moms[1] * q);
	K3-= (moms[0] * q) * (moms[1] * moms[2]);

	K4 = (moms[0] * q) * (moms[1] * q) + Q2 * K1/(2*pow(this->in_hadron->get_mass(), 2));

	K5 = (moms[0] * moms[2]) * (moms[1] * q);
	K5+= (moms[1] * moms[2]) * (moms[0] * q);
	K5-= (q * moms[2]) * K1/(2*pow(this->in_hadron->get_mass(), 2));

	//Dipole Form factors as in Benhar2007
	double FA, GE,FP;
	//Derived Form factors
	double GM, F1, F2;
	double GD_i = this->GD_i->calc_ff(Q2);
	double GD_f = this->GD_f->calc_ff(Q2);
	FA = this->FA->calc_ff(Q2);

	GE = this->in_hadron->get_charge()*GD_i - this->out_hadron->get_charge()*GD_f;
	GM = this->in_hadron->get_m_dipole()*GD_i-this->out_hadron->get_m_dipole()*GD_f;
	FP = 2*pow(this->in_hadron->get_mass(),2)*FA/(pow(this->M_pion, 2)+Q2);

	F1 = (GE + tau*GM)/(1+tau);
	F2 = (GM + GE)/(1+tau);

	//Define combined form factors
	double W1, W2, W3, W4, W5;

	W1 = 2 * (0.5 * Q2 * pow((F1 + F2),2) + (2*pow(this->in_hadron->get_mass(),2) + Q2/2)*pow(FA, 2));

	W2 = 4 * (pow(F1,2) + pow(FA,2) + pow(F2,2)*Q2/(4*pow(this->in_hadron->get_mass(),2)));
	if(this->in_neutrino->get_pdg_int() > 0){
		W3 = -4 * (F1 + F2) * FA;
	}else{
		W3 = 4 * (F1 + F2) * FA;
	}

	W4 = -2 * (F1*F2 + (2*pow(this->out_hadron->get_mass(),2) -0.5*Q2)*pow(F2,2)/(4*pow(this->in_hadron->get_mass(),2)) - 0.5*Q2*pow(FP,2) - 2*this->in_hadron->get_mass()*FA*FP);

	W5 = W2/2;

	return (pow(G_F, 2)/2)*(K1*W1 + K2*W2 + K3*W3 + K4*W4 + K5*W5);
}
}



#endif /* ME_2_QECC_H_ */
