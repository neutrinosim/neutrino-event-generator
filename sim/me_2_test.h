/*
 * me_2_test.h
 *
 *  Created on: 16 Apr 2021
 *      Author: jack
 */

#ifndef ME_2_TEST_H_
#define ME_2_TEST_H_


//Test matrix element squared for electron-muon photon scattering


#include "../utils/Physics/me_2.h"
#include "../utils/Physics/flavour.h"
#include "../utils/Maths/vec4.h"

using namespace UTILS;

namespace SIM{

class ME_2_test: public UTILS::ME_2{
private:

	UTILS::Flavour *in_electron, *in_hadron;


public:

	ME_2_test(const Flavour& lepton, const Flavour& hadron): ME_2(lepton, hadron, lepton){
		this->in_electron = new UTILS::Flavour(lepton);
		this->in_hadron = new UTILS::Flavour(hadron);
	}

	~ME_2_test(){}

	double const calc_ME_2(const std::vector<Vec4<double>> moms){

		double me_2;

		Vec4<double> q = moms[0] - moms[1];

		me_2 = (moms[1]*moms[3])*(moms[0]*moms[2]) + (moms[1]*moms[2])*(moms[0]*moms[3]); //Quarks and Leptons eq 6.27

		me_2 *= 8./pow(q*q, 2);

		return me_2;
	}

};
}



#endif /* ME_2_TEST_H_ */
