/*
 * me_2_point.h
 *
 *  Created on: 1 Dec 2020
 *      Author: jack
 */

#ifndef ME_2_POINT_H_
#define ME_2_POINT_H_

//point matrix element squared for electron-muon photon scattering


#include "../utils/Physics/me_2.h"
#include "../utils/Physics/flavour.h"
#include "../utils/Maths/vec4.h"

using namespace UTILS;

namespace SIM{

class ME_2_point: public UTILS::ME_2{
private:

	UTILS::Flavour *in_electron, *in_hadron;


public:

	ME_2_point(const Flavour& lepton, const Flavour& hadron): ME_2(lepton, hadron, lepton){
		this->in_electron = new UTILS::Flavour(lepton);
		this->in_hadron = new UTILS::Flavour(hadron);
	}

	~ME_2_point(){}

	double const calc_ME_2(const std::vector<Vec4<double>> moms){

		double me_2;

		Vec4<double> k_i = moms[0];
		Vec4<double> k_f = moms[1];
		Vec4<double> p_i = moms[2];
		Vec4<double> p_f = moms[3];

		me_2 = (moms[1]*moms[3])*(moms[0]*moms[2]) + (moms[1]*moms[2])*(moms[0]*moms[3]); //Quarks and Leptons eq 6.27
		me_2 -= pow(this->in_hadron->get_mass(),2) * (moms[1]*moms[0]);
		//me_2 -= pow(this->in_electron.get_mass(),2) * (p_f*p_i);
		//me_2 += 2*pow(this->in_electron.get_mass(),2)*pow(this->in_hadron.get_mass(),2);

		me_2 *= 8. /pow((k_i-k_f)*(k_i-k_f), 2);


		return me_2;
	}

};
}
#endif /* ME_2_POINT_H_ */
