/*
 * Hyperons.h
 *
 *  Created on: 13 Mar 2021
 *      Author: jack
 */

#ifndef HYPERON_PROD_H_
#define HYPERON_PROD_H_

#include "QECC.h"
#include "me_2_hyperon.h"

namespace SIM{

class HyperonProd: public QECC{

private:

	ME_2_HYPERON *mat_el_2;

public:

	HyperonProd(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hyperon,
			const Variable<double>& neutrino_spectrum, const Variable<double>& costheta,const double M_V, const double M_A, const double M_M);

	double calc_diffcross(const std::vector<Vec4<double>> moms);

};

HyperonProd::HyperonProd(const Flavour& in_lepton, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hyperon,
		const Variable<double>& neutrino_spectrum, const Variable<double>& costheta,const double M_V, const double M_A, const double M_M):
			QECC(in_lepton, in_hadron, out_lepton, out_hyperon, neutrino_spectrum, costheta, M_V, M_V, M_A){

	this->mat_el_2 = new ME_2_HYPERON(in_lepton, in_hadron, out_lepton, out_hyperon, M_V, M_A, M_M);

}

double HyperonProd::calc_diffcross(const std::vector<Vec4<double>> moms){


	double diff_cross = pow(2.*this->in_hadron->get_mass(),-2) * pow(4.*M_PI,-2) * pow(moms[1][0]/moms[0][0],2);
	diff_cross *= this->mat_el_2->calc_ME_2(moms);

	return diff_cross;
}

}

#endif /* HYPERON_PROD_H_ */
