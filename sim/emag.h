/*
 * emag.h
 *
 *  Created on: 19 Feb 2021
 *      Author: jack
 */

#ifndef EMAG_H_
#define EMAG_H_

#include "../utils/Physics/process.h"
#include "../utils/Physics/flavour.h"
#include "../utils/Physics/form_factor.h"
#include "me_2_emag.h"
#include "../ext/result_writer.h"
#include <vector>

using namespace UTILS;

namespace SIM{

class emag_process: public Process{

	UTILS::Flavour in_electron = UTILS::Flavour(11);
	UTILS::Flavour in_hadron = UTILS::Flavour(2212);

	ME_2_emag *me_2;

public:

	emag_process(const Variable<double>& electron_spectrum, double M_V, const Variable<double>& costheta): Process(in_electron, in_hadron, in_electron, electron_spectrum, costheta){

		me_2 = new ME_2_emag(this->in_electron, this->in_hadron, this->in_electron, this->in_hadron, M_V);

		std::vector<Variable<double>> addit_vars;
		addit_vars.emplace_back(0.0, 2*M_PI);
		this->p_space->add_variables(addit_vars);

	}

	virtual ~emag_process(){};

	//Get random point in phase space (returns 4momentum)
	std::vector<Vec4<double>> get_particle_moms(){

		double E_neutrino, p_neutrino, E_lepton, p_lepton, cos_theta, sin_theta, phi;
		std::vector<double> point = this->p_space->get_point();

		p_neutrino = point[0];
		cos_theta = point[1];
		sin_theta = pow(1-pow(cos_theta,2), 0.5);
		phi = point[2];

		E_neutrino = sqrt(pow(this->in_electron.get_mass(),2) + pow(p_neutrino, 2));

		E_lepton = E_neutrino/(1 + E_neutrino*(1-cos_theta)/this->in_hadron.get_mass());
		p_lepton = sqrt(pow(E_lepton, 2) - pow(this->in_electron.get_mass(),2));

		Vec4<double> k_i, k_f, p_i, p_f;
		//calculate incoming neutrino lepton 4mom
		k_i = Vec4<double>(E_neutrino, 0, 0, p_neutrino);
		//calculate incoming hadron 4mom
		p_i = Vec4<double>(this->in_hadron.get_mass(), 0, 0, 0);

		//calculate outgoing lepton 4mom
		k_f = Vec4<double>(E_lepton,p_lepton*sin_theta*std::cos(phi),p_lepton*sin_theta*std::sin(phi), p_lepton*cos_theta);
		//calculate outgoing hadron 4mom
		//p_f = Vec4<double>(1 ,-1*p_lepton*sin_theta*std::cos(phi),-1*p_lepton*sin_theta*std::sin(phi), p_neutrino - p_lepton*cos_theta);
		//double E_f = sqrt(pow(this->in_hadron.get_mass(),2) + pow(p_f.get_3mag(), 2));
		//p_f = Vec4<double>(E_f ,-1*p_lepton*sin_theta*std::cos(phi),-1*p_lepton*sin_theta*std::sin(phi), p_neutrino - p_lepton*cos_theta);
		p_f = (k_i + p_i) - k_f;

		std::vector<Vec4<double>> moms;

		moms.push_back(k_i);
		moms.push_back(k_f);
		moms.push_back(p_i);
		moms.push_back(p_f);

		return moms;
	}

	double calc_diffcross(const std::vector<Vec4<double>> moms){

		Vec4<double> k_i = moms[0];
		Vec4<double> k_f = moms[1];
		Vec4<double> p_i = moms[2];
		//Vec4<double> p_f = moms[3];
		//(k_i.get_3mag() / (2*M_PI*k_f.get_3mag()))

		double k = pow(2.*this->in_hadron.get_mass(),-2) * pow(4.*M_PI,-2) * pow(k_f[0]/k_i[0],2);
		double diff_cross = k*this->me_2->calc_ME_2(moms);

		return diff_cross;

	}
private:
	HepMC3::GenVertexPtr gen_interaction(const std::vector<Vec4<double>> moms){

		Vec4<double> k_i = moms[0];
		Vec4<double> k_f = moms[1];
		Vec4<double> p_i = moms[2];
		Vec4<double> p_f = moms[3];

		HepMC3::FourVector i_lep_mom = HepMC3::FourVector(k_i[1], k_i[2], k_i[3], k_i[0]);
		HepMC3::FourVector o_lep_mom = HepMC3::FourVector(k_f[1], k_f[2], k_f[3], k_f[0]);
		HepMC3::FourVector i_had_mom = HepMC3::FourVector(p_i[1], p_i[2], p_i[3], p_i[0]);
		HepMC3::FourVector o_had_mom = HepMC3::FourVector(p_f[1], p_f[2], p_f[3], p_f[0]);

		HepMC3::GenParticlePtr i_lep(new HepMC3::GenParticle(i_lep_mom, this->in_electron.get_pdg_int(), 4));
		HepMC3::GenParticlePtr o_lep(new HepMC3::GenParticle(o_lep_mom, this->in_electron.get_pdg_int(), 1));
		HepMC3::GenParticlePtr i_had(new HepMC3::GenParticle(i_had_mom, this->in_hadron.get_pdg_int(), 4));
		HepMC3::GenParticlePtr o_had(new HepMC3::GenParticle(o_had_mom, this->in_hadron.get_pdg_int(), 1));

		HepMC3::GenVertexPtr interaction(new HepMC3::GenVertex());

		interaction->add_particle_in(i_lep);
		interaction->add_particle_in(i_had);
		interaction->add_particle_out(o_lep);
		interaction->add_particle_out(o_had);

		return interaction;

	}

};
}



#endif /* EMAG_H_ */
