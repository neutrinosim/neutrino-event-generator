 /*
 * QECC.h
 *
 *  Created on: 30 Sep 2020
 *      Author: jack
 */

#ifndef SIM_QECC_H_
#define SIM_QECC_H_

#include "../utils/Physics/process.h"
#include "../utils/Physics/flavour.h"
#include "../utils/Physics/form_factor.h"
#include "me_2_qecc.h"
#include <vector>
#include "../ext/result_writer.h"

using namespace UTILS;

namespace SIM{

class QECC: public Process{
private:

	Flavour *out_hadron;

	ME_2_QECC *mat_el_2;

public:

	QECC(const Flavour& in_neutrino, const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, const Variable<double>& neutrino_spectrum, const Variable<double>& costheta,
			const double M_V_i, const double M_V_f, const double M_A);

	virtual ~QECC(){};

	//Get random point in phase space (returns 4momentum)
	std::vector<Vec4<double>> get_particle_moms();

	//Calculates differential cross-section for random point in phase space
	double calc_diffcross(const std::vector<Vec4<double>> moms);

private:

	//Check if point is physical, if not ME2=0
	bool check_point();

	//temporary place for constants
	const double g_A = -1.267;//ref: Leitner2005

	HepMC3::GenVertexPtr gen_interaction(const std::vector<Vec4<double>> moms);

};


QECC::QECC(const Flavour& in_neutrino,  const Flavour& in_hadron, const Flavour& out_lepton, const Flavour& out_hadron, const Variable<double>& neutrino_spectrum, const Variable<double>& costheta
		, const double M_V_i, const double M_V_f, const double M_A):
	Process(in_neutrino, in_hadron, out_lepton, neutrino_spectrum, costheta){

	this->out_hadron = new Flavour(out_hadron);

	this->mat_el_2 = new ME_2_QECC(in_neutrino, in_hadron, out_lepton, out_hadron, M_V_i, M_V_f, M_A);
	//variables are:  phi
	std::vector<Variable<double>> addit_vars;
	addit_vars.emplace_back(0.0, 2*M_PI);
	this->p_space->add_variables(addit_vars);
}

std::vector<Vec4<double>> QECC::get_particle_moms(){
	double E_neutrino, p_neutrino, E_lepton, p_lepton, cos_theta, sin_theta, phi;
	std::vector<double> point = this->p_space->get_point();

	p_neutrino = point[0];
	cos_theta = point[1];
	sin_theta = pow(1-pow(cos_theta,2), 0.5);
	phi = point[3];

	E_neutrino = pow(pow(this->in_neutrino->get_mass(), 2) + pow(p_neutrino, 2), 0.5);

	E_lepton = (pow(this->in_hadron->get_mass(),2)-pow(this->out_hadron->get_mass(),2)+2*E_neutrino*this->in_hadron->get_mass());
	E_lepton *= 1/(2*(this->in_hadron->get_mass() - E_neutrino*(cos_theta-1)));
	p_lepton = sqrt(pow(E_lepton, 2) - pow(this->in_neutrino->get_mass(),2));

	Vec4<double> k_i, k_f, p_i;
	//calculate incoming neutrino and outgoing lepton 4mom
	k_i = Vec4<double>(E_neutrino, 0, 0, p_neutrino);
	k_f = Vec4<double>(E_lepton,p_lepton*sin_theta*std::cos(phi),p_lepton*sin_theta*std::sin(phi), p_lepton*cos_theta);
	//calculate incoming/outgoing hadron 4mom
	p_i = Vec4<double>(this->in_hadron->get_mass(), 0, 0, 0);

	Vec4<double> p_f = k_i + p_i - k_f;

	std::vector<Vec4<double>> moms;

	moms.push_back(k_i);
	moms.push_back(k_f);
	moms.push_back(p_i);
	moms.push_back(p_f);

	return moms;

}
bool QECC::check_point(){
	return true;

}

double QECC::calc_diffcross(const std::vector<Vec4<double>> moms){

	double diff_cross = pow(2.*this->in_hadron->get_mass(),-2) * pow(4.*M_PI,-2) * pow(moms[1][0]/moms[0][0],2);
	diff_cross *= this->mat_el_2->calc_ME_2(moms);

	return diff_cross;
}


HepMC3::GenVertexPtr QECC::gen_interaction(const std::vector<Vec4<double>> moms){

	HepMC3::FourVector i_lep_mom = HepMC3::FourVector(moms[0][1], moms[0][2], moms[0][3], moms[0][0]);
	HepMC3::FourVector o_lep_mom = HepMC3::FourVector(moms[1][1], moms[1][2], moms[1][3], moms[1][0]);
	HepMC3::FourVector i_had_mom = HepMC3::FourVector(moms[2][1], moms[2][2], moms[2][3], moms[2][0]);

	if(moms[3][0] < this->out_hadron->get_mass()){
		std::cout<<"Non-physical nucleon created";
	}

	HepMC3::FourVector o_had_mom = HepMC3::FourVector(moms[3][1], moms[3][2], moms[3][3], moms[3][0]);

	HepMC3::GenParticlePtr i_lep(new HepMC3::GenParticle(i_lep_mom, this->in_neutrino->get_pdg_int(), 4));
	HepMC3::GenParticlePtr o_lep(new HepMC3::GenParticle(o_lep_mom, this->out_lepton->get_pdg_int(), 1));
	HepMC3::GenParticlePtr i_had(new HepMC3::GenParticle(i_had_mom, this->in_hadron->get_pdg_int(), 4));
	HepMC3::GenParticlePtr o_had(new HepMC3::GenParticle(o_had_mom, this->out_hadron->get_pdg_int(), 1));

	HepMC3::GenVertexPtr interaction(new HepMC3::GenVertex());

	interaction->add_particle_in(i_lep);
	interaction->add_particle_in(i_had);
	interaction->add_particle_out(o_lep);
	interaction->add_particle_out(o_had);

	return interaction;

	}
}


#endif /* SIM_QECC_H_ */
