/*
 * main.cpp
 *
 *  Created on: 2 Oct 2020
 *      Author: jack
 */

#include "sim/QECC.h"
#include "sim/emag.h"
#include "sim/hyperon_prod.h"
#include "sim/Resonances/resonances.h"
#include "sim/point_process.h"
#include "sim/test_process.h"

#include "utils/Maths/phase_space.h"
#include "utils/Physics/flavour.h"

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>

#include "HepMC3/ReaderAsciiHepMC2.h"
#include "HepMC3/GenEvent.h"

using namespace SIM;
using namespace EXT;
using namespace UTILS;
using namespace std;

vector<double> phase_map(vector<double> x){
	vector<double> vals = {x[0], x[1], 1.28, 0.0, x[2], x[3]};

	return vals;
}

bool boundary(vector<double> x){

	if(x[1]<=x[0]){return true;}
	else{return false;}
}
bool weighting(vector<double> x){
	return true;
}

int main(int argc, char *argv[]){
	cout<<"Initialising..."<<"\n";

	int n_events;
	string out_dir;

	n_events = 600000;
	out_dir = "./";

	double M_V = 0.98;
	double M_A = 1.025;
	double M_M = 0.71;

	//Monochrom_Variable<double> neutrino_spectrum(1.0);
	Monochrom_Variable<double> electron_spectrum(2.);

	Variable<double> costheta(-1, 0.999);

	Flavour e_neutrino = Flavour(12);
	Flavour electron = Flavour(11);
	Flavour proton = Flavour(2212);
	Flavour neutron = Flavour(2112);
	Flavour lambda = Flavour(3122);
	Flavour pion = Flavour(pion_p);

	//emag_process emag = emag_process(electron_spectrum, M_V, costheta);

	//point_process point = point_process(electron_spectrum, costheta);

	//QECC qecc = QECC(e_neutrino, proton, electron, neutron, electron_spectrum, costheta, 0.84, 0.84, 0.99);

	//HyperonProd hyp = HyperonProd(e_neutrino, proton, electron, lambda, electron_spectrum, costheta, M_V, M_A, M_M);

	Resonance res(e_neutrino, proton, e_neutrino, neutron, pion, true, NC, electron_spectrum, costheta, M_V, M_A);

	//cout << "Calculating cross section..."<<"\n";
	//HepMC3::GenCrossSection cross_sec = res.calc_cross(-3);
	//cout << "Initial Cross Section: " << cross_sec.xsec("Cross Section") <<"\n";

	cout<<"Generating events..."<<"\n";

	string outfile = "./test.hepmc";

	//emag.gen_events(n_events,outfile);

	//point.gen_events(n_events,outfile);

	//hyp.gen_events(n_events,outfile);

	//qecc.gen_events(n_events,outfile);

	res.gen_events(n_events, outfile);

	cout<<"Finished Event Generation. Validating..."<<"\n";

	HepMC3::ReaderAsciiHepMC2 reader(outfile);

	HepMC3::GenEvent evnt;

	bool b = reader.read_event(evnt);

	HepMC3::GenCrossSectionPtr cross = evnt.cross_section();
	double xsec = cross->xsec();
	double xsec_err = cross->xsec_err();

	cout<<"Was event read successfully? " << b <<"\n";
	cout<<"Cross Section:"<<xsec <<"\n";
	cout<<"Cross Section Error:"<<xsec_err<<"\n";
	cout<<"Percentage Error:" << (xsec_err/xsec)<<"\n";
	cout<<"Events stored in "<<outfile << "\n";
	cout<<"Events stored correctly. Execution finished."<<"\n";


	reader.close();

}

