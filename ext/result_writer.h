/*
 * result.h
 *
 *  Created on: 5 Oct 2020
 *      Author: jack
 */

#ifndef RESULT_WRITER_H_
#define RESULT_WRITER_H_

#include <vector>
#include <fstream>
#include "HepMC3/GenEvent.h"

namespace EXT{



template<typename Scalar>
struct Result{
	Scalar result;
	std::vector<Scalar> x;
	Result(Scalar r, std::vector<Scalar> X){result=r;x=X;}
	Result(const Result<Scalar>& res){result=res.result;x=res.x;}
	Result(std::istream& input,int Dim){
		Scalar point;
		for(int i = 0; i<Dim; i++){
			input.read((char*)&point, sizeof(Scalar));
			x.push_back(point);
		}
		input.read((char*)&this->result, sizeof(Scalar));
	}
	Result():result(0){}
	~Result(){};

	inline void writeout(std::ostream& output){
		int dim = int(x.size());
		for(int i=0; i<dim;i++){
			output.write((char*)&x[i], sizeof(Scalar));
		}
		output.write((char*)&result, sizeof(Scalar));
	}
	inline void readin(std::istream& input, size_t size){
		for(int i=0; i<x.size();i++){
			input.read((char*)&this->x[i], sizeof(Scalar));
		}
		input.read((char*)&this->result, sizeof(Scalar));
	}
};

}



#endif /* RESULT_WRITER_H_ */
