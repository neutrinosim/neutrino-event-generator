/*
 * dir_builder.h
 *
 *  Created on: 8 Oct 2020
 *      Author: jack
 *
 * Class that initialises an output directory tree
 * in the specified output folder. Stores paths for
 * the proper outputs of processes so that this does
 * not have to be handled manually.
 */

#ifndef DIR_BUILDER_H_
#define DIR_BUILDER_H_

#include <string>
#include <vector>

namespace EXT{

class Dir_builder{

private:

	std::string out_dir;
	std::vector<std::string>;

public:
	Dir_builder(std::string out_dir);

	std::string New_output();

};
}

#endif /* DIR_BUILDER_H_ */
